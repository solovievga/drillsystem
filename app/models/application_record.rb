class ApplicationRecord < ActiveRecord::Base
  include ValidationLogger
  self.abstract_class = true
end
