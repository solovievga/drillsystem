class DayReport < ApplicationRecord
  self.primary_key = 'day_report_id'
  belongs_to :user, foreign_key: 'users_id'
  belongs_to :spr_wellpad, foreign_key: 'spr_wellpads_id'
  belongs_to :spr_well_purpose, foreign_key: 'spr_well_purposes_id'
  belongs_to :spr_diameter, foreign_key: 'spr_diameters_id'
  belongs_to :spr_work_type_24, foreign_key: 'spr_work_types_id_24', class_name: 'SprWorkType'
  belongs_to :spr_work_type_04, foreign_key: 'spr_work_types_id_04', class_name: 'SprWorkType'
  belongs_to :spr_bit_type, foreign_key: 'spr_bit_types_id', optional: true
  belongs_to :spr_turbodrill_type, foreign_key: 'spr_turbodrill_types_id', optional: true
  belongs_to :spr_contract_solution, foreign_key: 'spr_contract_solutions_id'
  belongs_to :spr_telesystem, foreign_key: 'spr_telesystems_id', optional: true
  belongs_to :spr_drillrig_type, foreign_key: 'spr_drillrig_types_id'
  has_many :balance_times, foreign_key: 'dayreports_id', dependent: :delete_all
  has_many :requests, foreign_key: 'dayreports_id', dependent: :delete_all
  has_many :chocks, foreign_key: 'day_reports_id', dependent: :delete_all
  accepts_nested_attributes_for :balance_times, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :requests, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :chocks, reject_if: :all_blank, allow_destroy: true

  def as_json(options={})
    super(options).merge({user: user,
                          spr_oilfields_id: spr_wellpad.present? ? spr_wellpad.spr_oilfields_id : '',
                          balance_times_attributes: balance_times,
                          requests_attributes: requests,
                          chocks_attributes: chocks,
                          name: get_name,
                          sinking_month: get_sinking_month,
                          sinking_year: get_sinking_year,
                          distance: spr_wellpad.spr_oilfield.distance
                         })
  end

  def get_name
    "#{spr_wellpad.spr_oilfield.name} #{spr_wellpad.wellpad_number} #{well}"
  end

  def get_well_full_name
    "#{spr_wellpad.spr_oilfield.name} #{spr_wellpad.wellpad_number} #{well}"
  end

  def get_balance_time_work_type
    str = ''
    balance_times.each do |bt|
      str += "#{bt.spr_work_type.name} \n"
    end

    return str
  end

  def get_balance_time_field(field_name)
    str = ''
    balance_times.each do |bt|
      str += "#{bt.read_attribute(field_name) || '---'} \n"
    end

    return str
  end

  def get_request_type
    str = ''
    requests.each do |r|
      str += "#{r.spr_request.name} \n"
    end

    return str
  end

  def get_request_date_time
    str = ''
    requests.each do |r|
      str += "#{r.date_time.strftime('%d.%m.%y %H:%M') || '---'} \n"
    end

    return str
  end

  def get_bit_roller
    bit_is_roller ? '(шар.)' : ''
  end

  def get_sinking_month
    DayReport.select(:sinking_day).where("report_day >= ? AND report_day <= ? AND spr_wellpads_id = ? AND well = ?", report_day.beginning_of_month, report_day, spr_wellpads_id, well).sum(:sinking_day)
  end

  def get_sinking_year
    DayReport.select(:sinking_day).where("report_day >= ? AND report_day <= ? AND spr_wellpads_id = ? AND well = ?", report_day.beginning_of_year, report_day, spr_wellpads_id, well).sum(:sinking_day)
  end

  def solute_24
    "#{parse_float(specific_gravity_24)}-#{parse_float(viscosity_24)}-#{parse_float(water_separation_24)}-#{parse_float(percent_greasing_24)}"
  end

  def solute_04
    "#{parse_float(specific_gravity_04)}-#{parse_float(viscosity_04)}-#{parse_float(water_separation_04)}-#{parse_float(percent_greasing_04)}"
  end

  def parse_float(float_number)
    float_number.present? ? "%g" % ("%.2f" % float_number) : ''
  end

  def get_transkript_sinking_month
    day_reports = DayReport.where("report_day >= ? AND report_day <= ? AND spr_wellpads_id = ? AND well = ?", report_day.beginning_of_month, report_day, spr_wellpads_id, well)
    str = ''
    day_reports.each do |dr|
      str += "#{dr.get_name} #{dr.sinking_day} \n"
    end
    str
  end

  def get_chocks
    str = ''
    chocks.each do |chock|
      str += "#{chock.spr_chock_diameter.diameter}-#{parse_float(chock.chock_depth)} \n "
    end
    str
  end

end
