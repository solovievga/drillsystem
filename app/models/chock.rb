class Chock < ApplicationRecord
  self.primary_key = 'chock_id'
  belongs_to :spr_chock_diameter, foreign_key: 'spr_chock_diameters_id'
end
