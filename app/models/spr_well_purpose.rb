class SprWellPurpose < ApplicationRecord
  self.primary_key = 'well_purpose_id'

  def name
    short_name || full_name
  end
end
