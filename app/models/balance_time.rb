class BalanceTime < ApplicationRecord
  self.table_name = 'balance_time'
  self.primary_key = 'balance_time_id'

  belongs_to :spr_work_type, foreign_key: 'spr_work_types_id'
end
