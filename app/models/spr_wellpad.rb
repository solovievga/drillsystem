class SprWellpad < ApplicationRecord
  self.primary_key = 'wellpad_id'
  belongs_to :spr_oilfield, foreign_key: 'spr_oilfields_id'
end
