class Request < ApplicationRecord
  self.table_name = 'requests'
  self.primary_key = 'request_id'

  belongs_to :spr_request, foreign_key: 'spr_requests_id'
end
