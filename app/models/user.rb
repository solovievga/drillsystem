class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :trackable, :authentication_keys => [:login]
  include DeviseTokenAuth::Concerns::User

  before_create :set_default_role

  def as_json(options={})
    {id: id, name: name, nickname: nickname, role: role}
  end

  def check_permission(need_role)
    case need_role
      when :admin
        (self.role == :admin)
      when :manager
        (self.role == :admin || self.role == :manager)
      else
        raise 'У вас нет необходимых привелегий.'
    end
  end

  def isAdmin
    (role == 'admin')
  end

  def isManager
    (role == 'admin' || role == 'manager')
  end

  private

  def set_default_role
    self.role = :user
  end
end
