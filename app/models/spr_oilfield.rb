class SprOilfield < ApplicationRecord

  def name
    short_name || full_name
  end
end
