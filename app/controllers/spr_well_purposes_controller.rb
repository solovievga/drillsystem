class SprWellPurposesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_well_purpose, only: [:show, :update, :destroy]

  # GET /spr_well_purposes
  def index
    @spr_well_purposes = SprWellPurpose.all

    render json: @spr_well_purposes
  end

def show
  render json: @spr_well_purpose
end

def create
  @spr_well_purpose = SprWellPurpose.new(spr_well_purposes_params)

  if @spr_well_purpose.save
    render json: @spr_well_purpose, status: :created, location: @spr_well_purpose
  else
    render json: @spr_well_purpose.erros, status: :unprocessable_entity
  end
end

def update
  if @spr_well_purpose.update(spr_well_purposes_params)
    render json: @spr_well_purpose
  else
    render json: @spr_well_purpose.errors, status: :unprocessable_entity
  end
end

def destroy
  @spr_well_purpose.destroy
end

private
  def set_spr_well_purpose
    @spr_well_purpose = SprWellPurpose.find(params[:id])
  end

# Only allow a trusted parameter "white list" through.
def spr_well_purposes_params
  params.permit(
      :well_purpose_id,
      :full_name,
      :is_active,
      :short_name
  )
end
end
