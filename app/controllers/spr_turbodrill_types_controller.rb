class SprTurbodrillTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_turbodrill_type, only: [:show, :update, :destroy]

  def index
    @spr_turbodrill_types = SprTurbodrillType.all

    render json: @spr_turbodrill_types
  end

def show
  render json: @spr_turbodrill_type
end

def create
  @spr_turbodrill_type = SprTurbodrillType.new(spr_turbodrill_types_params)

  if @spr_turbodrill_type.save
    render json: @spr_turbodrill_type, status: :created, location: @spr_turbodrill_type
  else
    render json: @spr_turbodrill_type.erros, status: :unprocessable_entity
  end
end

def update
  if @spr_turbodrill_type.update(spr_turbodrill_types_params)
    render json: @spr_turbodrill_type
  else
    render json: @spr_turbodrill_type.errors, status: :unprocessable_entity
  end
end

def destroy
  @spr_turbodrill_type.destroy
end

private
  def set_spr_turbodrill_type
    @spr_turbodrill_type = SprTurbodrillType.find(params[:id])
  end

# Only allow a trusted parameter "white list" through.
def spr_turbodrill_types_params
  params.permit(
      :turbodrill_type_id,
      :name,
      :is_active
  )
end
end
