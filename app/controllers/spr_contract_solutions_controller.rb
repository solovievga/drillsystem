class SprContractSolutionsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_spr_contract_solution, only: [:show, :update, :destroy]
  
    # GET /spr_contract_solutions
    def index
      @spr_contract_solutions = SprContractSolution.all
  
      render json: @spr_contract_solutions
    end
  
    def show
      render json: @spr_contract_solution
    end
  
    def create
      @spr_contract_solution = SprContractSolution.new(spr_contract_solutions_params)
  
      if @spr_contract_solution.save
        render json: @spr_contract_solution, status: :created, location: @spr_contract_solution
      else
        render json: @spr_contract_solution.erros, status: :unprocessable_entity
      end
    end
  
    def update
      if @spr_contract_solution.update(spr_contract_solutions_params)
        render json: @spr_contract_solution
      else
        render json: @spr_contract_solution.errors, status: :unprocessable_entity
      end
    end
  
    def destroy
      @spr_contract_solution.destroy
    end
  
    private
      def set_spr_contract_solution
        @spr_contract_solution = SprContractSolution.find(params[:id])
      end
  
    # Only allow a trusted parameter "white list" through.
    def spr_contract_solutions_params
      params.permit(
          :contract_solution_id,
          :name
      )
    end
  end
