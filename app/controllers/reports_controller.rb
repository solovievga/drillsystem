class ReportsController < ApplicationController
  #before_action :authenticate_user!

  def svod
    @day_reports = DayReport.includes(spr_wellpad: :spr_oilfield).where(report_day: params[:report_day]).order('spr_oilfields.distance DESC')
    wb = RubyXL::Parser.parse("lib/reports/svod.xlsx")
    sheet = wb.worksheets[0]

    #change header
    sheet[0][0].change_contents(params[:report_day])
    sheet[1][0].change_contents('СУТОЧНАЯ СВОДКА ПО БУРЕНИЮ в ООО "МУБР" за ' + params[:report_day].to_s)

    #insert selected records
    @day_reports.each do |dr|
      row = sheet.insert_row(5)
      sheet.change_row_height(5, 60)
      row.cells[0].change_contents("#{dr.get_well_full_name} \n #{parse_float(dr.project_depth)} \n #{dr.user.name} - #{dr.user.nickname} \n #{dr.masters}")
      row.cells[1].change_contents("#{dr.spr_well_purpose.name} \n #{parse_float(dr.spr_diameter.diameter)} \n #{dr.get_chocks} #{parse_float(dr.v_absorbing)}")
      row.cells[2].change_contents("#{dr.drill_start.strftime('%d.%m.%y %H:%M')} \n #{dr.drill_end.strftime('%d.%m.%y %H:%M')} \n #{parse_float(dr.drill_start_deviation)}")
      row.cells[3].change_contents(" #{parse_float(dr.bottom_24)} \n #{dr.spr_work_type_24.name} \n #{dr.solute_24}")
      row.cells[4].change_contents("#{parse_float(dr.rest_oil)} \n #{parse_float(dr.rest_aqua)} \n #{parse_float(dr.rest_solution)}/#{parse_float(dr.presence_solution)}")
      row.cells[5].change_contents("#{dr.spr_turbodrill_type.name if dr.spr_turbodrill_type.present?} #{parse_float(dr.turbodrill_number)} \n #{parse_float(dr.next_angle)}")
      row.cells[6].change_contents("#{dr.spr_bit_type.name if dr.spr_bit_type.present?} #{parse_float(dr.bit_diameter)} #{parse_float(dr.get_bit_roller)} \n #{dr.bit_number} \n #{parse_float(dr.bit_work_hours)}")
      row.cells[7].change_contents("#{parse_float(dr.slotting_number)}")
      row.cells[8].change_contents("#{parse_float(dr.slotting_interval)}")
      row.cells[9].change_contents("#{dr.get_balance_time_work_type} - #{dr.get_balance_time_field('num_value')}")
      row.cells[10].change_contents("#{dr.get_balance_time_field('time_')}")
      row.cells[11].change_contents("#{dr.get_balance_time_field('culprit')}")
      row.cells[12].change_contents("#{parse_float(dr.bottom_04)} \n #{dr.spr_work_type_04.name} \n #{dr.solute_04}")
      row.cells[13].change_contents("#{parse_float(dr.sinking_day)} \n #{parse_float(dr.drill_speed_prg)}/#{parse_float(dr.drill_speed_fact)}")
      row.cells[14].change_contents("#{parse_float(dr.get_sinking_month)}")
      row.cells[15].change_contents("#{parse_float(dr.get_sinking_year)}")
      row.cells[16].change_contents("#{dr.get_transkript_sinking_month}")
      row.cells[17].change_contents("#{dr.get_request_type}")
      row.cells[18].change_contents("#{dr.get_request_date_time}")
      row.cells[19].change_contents("#{dr.spr_telesystem.name if dr.spr_telesystem.present?} \n #{dr.spr_drillrig_type.name if dr.spr_drillrig_type.present?} \n #{dr.spr_contract_solution.name if dr.spr_contract_solution.present?}")
    end

    #delete empty row
    sheet.delete_row(4)

    #send excel file
    send_data wb.stream.string, type: 'application/excel', disposition: 'attachment', filename: "svod_#{params[:report_day]}.xlsx"
  end

  def parse_float(float_number)
    begin
      float_number.present? ? "%g" % ("%.2f" % float_number) : ''
    rescue
      float_number
    end
  end
end