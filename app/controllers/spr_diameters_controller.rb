class SprDiametersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_diameter, only: [:show, :update, :destroy]

  # GET /spr_diameters
  def index
    @spr_diameters = SprDiameter.all

    render json: @spr_diameters
  end

  def show
    render json: @spr_diameter
  end

  def create
    @spr_diameter = SprDiameter.new(spr_diameters_params)

    if @spr_diameter.save
      render json: @spr_diameter, status: :created, location: @spr_diameter
    else
      render json: @spr_diameter.erros, status: :unprocessable_entity
    end
  end

  def update
    if @spr_diameter.update(spr_diameters_params)
      render json: @spr_diameter
    else
      render json: @spr_diameter.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @spr_diameter.destroy
  end

  private
    def set_spr_diameter
      @spr_diameter = SprDiameter.find(params[:id])
    end

  # Only allow a trusted parameter "white list" through.
  def spr_diameters_params
    params.permit(
        :diameter_id,
        :diameter,
        :is_active
    )
  end
end
