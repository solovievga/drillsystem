class SprChockDiametersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_chock_diameter, only: [:show, :update, :destroy]

  # GET /spr_chock_diameters
  def index
    @spr_chock_diameters = SprChockDiameter.all

    render json: @spr_chock_diameters
  end

  def show
    render json: @spr_chock_diameter
  end

  def create
    @spr_chock_diameter = SprChockDiameter.new(spr_chock_diameters_params)

    if @spr_chock_diameter.save
      render json: @spr_chock_diameter, status: :created, location: @spr_chock_diameter
    else
      render json: @spr_chock_diameter.erros, status: :unprocessable_entity
    end
  end

  def update
    if @spr_chock_diameter.update(spr_chock_diameters_params)
      render json: @spr_chock_diameter
    else
      render json: @spr_chock_diameter.errors, status: :unprocessable_entity
    end
  end


  def destroy
    @spr_chock_diameter.destroy
    head(204)
  end

  private
    def set_spr_chock_diameter
      @spr_chock_diameter = SprChockDiameter.find(params[:id])
    end

  # Only allow a trusted parameter "white list" through.
  def spr_chock_diameters_params
    params.permit(
    	:chock_diameter_id,
    	:diameter,
    	:rn
    )
  end
end
