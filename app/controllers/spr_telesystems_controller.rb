class SprTelesystemsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_telesystem, only: [:show, :update, :destroy]

  # GET /spr_telesystems
  def index
    @spr_telesystems = SprTelesystem.all

    render json: @spr_telesystems
  end

def show
  render json: @spr_telesystem
end

def create
  @spr_telesystem = SprTelesystem.new(spr_telesystems_params)

  if @spr_telesystem.save
    render json: @spr_telesystem, status: :created, location: @spr_telesystem
  else
    render json: @spr_telesystem.erros, status: :unprocessable_entity
  end
end

def update
  if @spr_telesystem.update(spr_telesystems_params)
    render json: @spr_telesystem
  else
    render json: @spr_telesystem.errors, status: :unprocessable_entity
  end
end

def destroy
  @spr_telesystem.destroy
end

private
  def set_spr_telesystem
    @spr_telesystem = SprTelesystem.find(params[:id])
  end

# Only allow a trusted parameter "white list" through.
def spr_telesystems_params
  params.permit(
      :telesystem_id,
      :name,
      :is_active
  )
end
end
