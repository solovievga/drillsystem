class SprDrillrigTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_drillrig_type, only: [:show, :update, :destroy]

  # GET /spr_drillrig_types
  def index
    @spr_drillrig_types = SprDrillrigType.all

    render json: @spr_drillrig_types
  end

def show
  render json: @spr_drillrig_type
end

def create
  @spr_drillrig_type = SprDrillrigType.new(spr_drillrig_types_params)

  if @spr_drillrig_type.save
    render json: @spr_drillrig_type, status: :created, location: @spr_drillrig_type
  else
    render json: @spr_drillrig_type.erros, status: :unprocessable_entity
  end
end

def update
  if @spr_drillrig_type.update(spr_drillrig_types_params)
    render json: @spr_drillrig_type
  else
    render json: @spr_drillrig_type.errors, status: :unprocessable_entity
  end
end

def destroy
  @spr_drillrig_type.destroy
end

private
  def set_spr_drillrig_type
    @spr_drillrig_type = SprDrillrigType.find(params[:id])
  end

# Only allow a trusted parameter "white list" through.
def spr_drillrig_types_params
  params.permit(
      :drillrig_type_id,
      :name,
      :is_active
  )
end
end

