class SprRequestsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_request, only: [:show, :update, :destroy]

  # GET /spr_telesystems
  def index
    @spr_requests = SprRequest.all

    render json: @spr_requests
  end

def show
  render json: @spr_request
end

def create
  @spr_request = SprRequest.new(spr_requests_params)

  if @spr_request.save
    render json: @spr_request, status: :created, location: @spr_request
  else
    render json: @spr_request.erros, status: :unprocessable_entity
  end
end

def update
  if @spr_request.update(spr_requests_params)
    render json: @spr_request
  else
    render json: @spr_request.errors, status: :unprocessable_entity
  end
end

def destroy
  @spr_request.destroy
end

private
  def set_spr_request
    @spr_request = SprRequest.find(params[:id])
  end

# Only allow a trusted parameter "white list" through.
def spr_requests_params
  params.permit(
      :name,
      :is_active,
      :request_id
  )
end
end
