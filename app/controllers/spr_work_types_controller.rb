class SprWorkTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_work_type, only: [:show, :update, :destroy]

  # GET /spr_work_types
  def index
    @spr_work_types = SprWorkType.all

    render json: @spr_work_types
  end

def show
  render json: @spr_work_type
end

def create
  @spr_work_type = SprWorkType.new(spr_work_types_params)

  if @spr_work_type.save
    render json: @spr_work_type, status: :created, location: @spr_work_type
  else
    render json: @spr_work_type.erros, status: :unprocessable_entity
  end
end

def update
  if @spr_work_type.update(spr_work_types_params)
    render json: @spr_work_type
  else
    render json: @spr_work_type.errors, status: :unprocessable_entity
  end
end

def destroy
  @spr_work_type.destroy
end

private
  def set_spr_work_type
    @spr_work_type = SprWorkType.find(params[:id])
  end

# Only allow a trusted parameter "white list" through.
def spr_work_types_params
  params.permit(
      :work_type_id,
      :name,
      :is_active,
      :full_name,
      :group_name
  )
end
end
