class SprOilfieldsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_oilfield, only: [:show, :update, :destroy]

  # GET /spr_oilfields
  def index
    @spr_oilfields = SprOilfield.all

    render json: @spr_oilfields
  end

def show
  render json: @spr_oilfield
end

def create
  @spr_oilfield = SprOilfield.new(spr_oilfields_params)

  if @spr_oilfield.save
    render json: @spr_oilfield, status: :created, location: @spr_oilfield
  else
    render json: @spr_oilfield.erros, status: :unprocessable_entity
  end
end

def update
  if @spr_oilfield.update(spr_oilfields_params)
    render json: @spr_oilfield
  else
    render json: @spr_oilfield.errors, status: :unprocessable_entity
  end
end

def destroy
  @spr_oilfield.destroy
end

private
  def set_spr_oilfield
    @spr_oilfield = SprOilfield.find(params[:id])
  end

# Only allow a trusted parameter "white list" through.
def spr_oilfields_params
  params.permit(
      :oilfield_id,
      :full_name,
      :short_name,
      :is_active,
      :distance
  )
end
end


