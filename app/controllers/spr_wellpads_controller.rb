class SprWellpadsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_wellpad, only: [:show, :update, :destroy]

  # GET /spr_wellpads
  def index
    @spr_wellpads = SprWellpad.all

    render json: @spr_wellpads
  end

  def references
    @spr_oilfields = SprOilfield.all 

    render json: {
      spr_oilfields: @spr_oilfields
    }
  end

def show
  render json: @spr_wellpad
end

def create
  @spr_wellpad = SprWellpad.new(spr_wellpads_params)

  if @spr_wellpad.save
    render json: @spr_wellpad, status: :created, location: @spr_wellpad
  else
    render json: @spr_wellpad.erros, status: :unprocessable_entity
  end
end

def update
  if @spr_wellpad.update(spr_wellpads_params)
    render json: @spr_wellpad
  else
    render json: @spr_wellpad.errors, status: :unprocessable_entity
  end
end

def destroy
  @spr_wellpad.destroy
end

private
  def set_spr_wellpad
    @spr_wellpad = SprWellpad.find(params[:id])
  end

# Only allow a trusted parameter "white list" through.
def spr_wellpads_params
  params.permit(
      :wellpad_id,
      :wellpad_number,
      :spr_oilfields_id,
      :is_active
  )
end
end
