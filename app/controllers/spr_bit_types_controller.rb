class SprBitTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_spr_bit_type, only: [:show, :update, :destroy]

  # GET /spr_bit_types
  def index
    @spr_bit_types = SprBitType.all
    
    render json: @spr_bit_types
  end

  def show
    render json: @spr_bit_type
  end

  def create
    @spr_bit_type = SprBitType.new(spr_bit_type_params)

    if @spr_bit_type.save
      render json: @spr_bit_type, status: :created, location: @spr_bit_type
    else
      render json: @spr_bit_type.erros, status: :unprocessable_entity
    end
  end

  def update
    if @spr_bit_type.update(spr_bit_type_params)
      render json: @spr_bit_type
    else
      render json: @spr_bit_type.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @spr_bit_type.destroy
  end

  private
    def set_spr_bit_type
      @spr_bit_type = SprBitType.find(params[:id])
    end

  # Only allow a trusted parameter "white list" through.
  def spr_bit_type_params
    params.permit(
    	:bit_type_id,
    	:name,
    	:is_active,
    	:rn
    )
  end
end
