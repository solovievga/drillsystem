class DayReportsController < ApplicationController
  before_action :set_day_report, only: [:show, :update, :destroy]
  before_action :validate_access, only: [:create, :update, :destroy]


  # GET /day_reports
  def index
    @day_reports = (current_user.role == 'user') ? DayReport.includes(:user, :balance_times, :requests, :chocks, spr_wellpad: :spr_oilfield).where(users_id: current_user.id, report_day: params[:report_day]).order(report_day: :desc)
                       : DayReport.includes(:user, :balance_times, :requests, :chocks, spr_wellpad: :spr_oilfield).where(report_day: params[:report_day]).order(report_day: :desc)

    render json: @day_reports
  end

  def test
    render json: {test: '123456'}
  end

  def references
    @spr_bit_types = SprBitType.all
    @spr_diameters = SprDiameter.all
    @users = User.where(role: 'user')
    @spr_drillrig_types = SprDrillrigType.all
    @spr_telesystems = SprTelesystem.all
    @spr_turbodrill_types = SprTurbodrillType.all
    @spr_well_purposes = SprWellPurpose.all
    @spr_wellpads = SprWellpad.all
    @spr_work_types = SprWorkType.all.order(is_active: :desc)
    @spr_oilfields = SprOilfield.all
    @spr_requests = SprRequest.all
    @spr_contract_solutions = SprContractSolution.all
    @spr_chock_diameters = SprChockDiameter.all

    render json: {
        spr_bit_types: @spr_bit_types,
        spr_diameters: @spr_diameters,
        users: @users,
        spr_drillrig_types: @spr_drillrig_types,
        spr_telesystems: @spr_telesystems,
        spr_turbodrill_types: @spr_turbodrill_types,
        spr_well_purposes: @spr_well_purposes,
        spr_wellpads: @spr_wellpads,
        spr_work_types: @spr_work_types,
        spr_oilfields: @spr_oilfields,
        spr_requests: @spr_requests,
        spr_contract_solutions: @spr_contract_solutions,
        spr_chock_diameters: @spr_chock_diameters
    }
  end

  # GET /day_reports/1
  def show
    render json: @day_report
  end

  # POST /day_reports
  def create
    @day_report = DayReport.new(day_report_params)
    @day_report.users_id = current_user.isManager ? params[:users_id] : current_user.id

    if @day_report.save
      render json: @day_report, status: :created, location: @day_report
    else
      render json: @day_report.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /day_reports/1
  def update
    @day_report.users_id = current_user.isManager ? params[:users_id] : current_user.id
    if @day_report.update(day_report_params)
      render json: @day_report
    else
      render json: @day_report.errors, status: :unprocessable_entity
    end
  end

  # DELETE /day_reports/1
  def destroy
    @day_report.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_day_report
    @day_report = DayReport.find(params[:id])
  end

  def validate_access
    if current_user.role == 'user'
      date = @day_report.present? ? @day_report.report_day : DateTime.parse(params[:report_day])
      current_date = DateTime.now
      current_time = DateTime.now.strftime('%H:%M')

      if !(current_date.ago(4.hours).strftime('%d.%m.%Y') == date.strftime('%d.%m.%Y') or
          current_date.strftime('%d.%m.%Y') == date.strftime('%d.%m.%Y'))
        render json: {error: 'Данные закрыты для редактирования.'}, status: :locked
      end
    end
  end

  # Only allow a trusted parameter "white list" through.
  def day_report_params
    params.permit(
        :report_day,
        :spr_wellpads_id,
        :well,
        :project_depth,
        :layer,
        :spr_well_purposes_id,
        :spr_diameters_id,
        :v_absorbing,
        :drill_start,
        :drill_start_deviation,
        :spr_work_types_id_24,
        :bottom_24,
        :rest_oil,
        :rest_aqua,
        :slotting_number,
        :slotting_interval,
        :spr_work_types_id_04,
        :bottom_04,
        :sinking_day,
        :sinking_month,
        :sinking_year,
        :bit_diameter,
        :spr_turbodrill_types_id,
        :turbodrill_number,
        :spr_telesystems_id,
        :spr_drillrig_types_id,
        :spr_drill_teams_id,
        :spr_bit_types_id,
        :drill_end,
        :next_angle,
        :specific_gravity_24,
        :viscosity_24,
        :water_separation_24,
        :percent_greasing_24,
        :specific_gravity_04,
        :viscosity_04,
        :water_separation_04,
        :percent_greasing_04,
        :rest_solution,
        :presence_solution,
        :spr_contract_solutions_id,
        :bit_is_roller,
        :masters,
        :bit_number,
        :bit_work_hours,
        :drill_speed_prg,
        :drill_speed_fact,
        :val24,
        :val04,
        balance_times_attributes: [:id, :spr_work_types_id, :time_, :culprit, :num_value, :_destroy],
        requests_attributes: [:id, :spr_requests_id, :date_time, :_destroy],
        chocks_attributes: [:id, :spr_chock_diameters_id, :chock_depth, :_destroy]
    )
  end
end
