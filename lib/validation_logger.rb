module ValidationLogger
  extend ActiveSupport::Concern

  included do
    after_validation :log_errors, :if => Proc.new {|m| m.errors}
  end

  def log_errors
    Rails.logger.warn errors.full_messages.join("\n") unless errors.empty?
  end
end