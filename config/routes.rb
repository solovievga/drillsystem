Rails.application.routes.draw do
  resources :spr_requests, only: [:index, :create, :update, :destroy]
  resources :spr_oilfields, only: [:index, :create, :update, :destroy]
  resources :day_reports, only: [:index, :create, :update, :destroy]
  resources :spr_chock_diameters, only: [:index, :create, :update, :destroy]
  resources :spr_turbodrill_types, only: [:index, :create, :update, :destroy]
  resources :spr_bit_types, only: [:index, :create, :update, :destroy]
  resources :spr_drill_teams, only: [:index, :create, :update, :destroy]
  resources :spr_drillrig_types, only: [:index, :create, :update, :destroy]
  resources :spr_telesystems, only: [:index, :create, :update, :destroy]
  resources :spr_work_types, only: [:index, :create, :update, :destroy]
  resources :spr_diameters, only: [:index, :create, :update, :destroy]
  resources :spr_well_purposes, only: [:index, :create, :update, :destroy]
  resources :spr_wellpads, only: [:index, :create, :update, :destroy]
  resources :spr_contract_solutions, only: [:index, :create, :update, :destroy]
  get 'day_reports/references' => 'day_reports#references'
  get 'spr_wellpads/references' => 'spr_wellpads#references'
  mount_devise_token_auth_for 'User', at: 'auth'

  namespace :reports do
    get 'svod'
  end


  get 'test' => 'day_reports#test'
  # get 'reports/svod' => 'reports#svod'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
