Devise.setup do |config|
  config.authentication_keys = [:nickname]
  config.password_length = 4..128
  config.secret_key = '9bf50e97ef15a0288ba027f25f046e2a12958c9bddac675a2007a0d280c6ec40480be36488d206a934459732ea7f563c93c5e054cde49b8385142eb8ed804a44'
end