# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170518043248) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "b_requests", id: false, force: :cascade do |t|
    t.integer "b_dayreports_id",                                         null: false
    t.integer "spr_requests_id",                                         null: false
    t.decimal "time_",           precision: 6, scale: 2, default: "0.0", null: false
    t.index ["b_dayreports_id"], name: "requests_dayreports_fki", using: :btree
    t.index ["spr_requests_id"], name: "brequests_sprrequest_fki", using: :btree
  end

  create_table "d_balance_time", id: false, force: :cascade do |t|
    t.integer "d_dayreports_id",                                          null: false
    t.integer "spr_worktypes_id",                                         null: false
    t.decimal "time_",            precision: 6, scale: 2, default: "0.0", null: false
    t.index ["d_dayreports_id"], name: "balancetime_dayreport_fki", using: :btree
    t.index ["spr_worktypes_id"], name: "balancetime_worktype_fki", using: :btree
  end

  create_table "d_dayreports", primary_key: "dayreport_id", id: :integer, default: -> { "nextval('d_dayreports_seq'::regclass)" }, force: :cascade, comment: "Скважина" do |t|
    t.date     "reportday",                                                            null: false, comment: "День за которую подается сводка"
    t.integer  "spr_wellpads_id",                                                      null: false, comment: "Куст на котором проходит бурение"
    t.string   "well",                  limit: 10,                                     null: false
    t.integer  "project_depth",                                            default: 0, null: false, comment: "Проектная глубина"
    t.string   "layer",                 limit: 10,                                     null: false, comment: "Пласт"
    t.integer  "spr_wellpurposes_id",                                                  null: false, comment: "Назначение скважины справочник"
    t.integer  "spr_diameters_id",                                                     null: false, comment: "Диаметр справочник"
    t.integer  "v_absorbing",                                                          null: false, comment: "Объем поглощения"
    t.datetime "drill_start",                                                          null: false, comment: "Дата и время начала бурения"
    t.decimal  "drill_start_deviation",            precision: 6, scale: 2,             null: false, comment: "Отклоенние от времени начала бурения"
    t.integer  "spr_worktypes_id_24",                                                  null: false, comment: "Состояние на 24 часа вид работ"
    t.string   "solute_24",             limit: 20,                                     null: false, comment: "Состояние на 24 часа раствор"
    t.integer  "bottom_24",                                                            null: false, comment: "Состояние на 24 часа забой"
    t.decimal  "rest_oil",                         precision: 9, scale: 2,             null: false, comment: "Остаток нефти"
    t.decimal  "rest_aqua",                        precision: 9, scale: 2,             null: false, comment: "Остаток промывочной жидкости"
    t.integer  "slotting_number",                                                      null: false, comment: "Номер долбления"
    t.string   "slotting_interval",     limit: 50,                                     null: false, comment: "Интервал долбления"
    t.integer  "spr_worktypes_id_04",                                                  null: false, comment: "Состояние на 4 часа вид работ"
    t.string   "solute_04",             limit: 20,                                     null: false, comment: "Состояние на 4 часа раствор"
    t.integer  "bottom_04",                                                            null: false, comment: "Состояние на 44 часа забой"
    t.integer  "sinking_day",                                                          null: false, comment: "Проходка день"
    t.integer  "sinking_month",                                                        null: false, comment: "Проходка месяц"
    t.integer  "sinking_year",                                                         null: false, comment: "Проходка год"
    t.integer  "spr_telesystems_id",                                                   null: false, comment: "Телесистема справочник"
    t.integer  "spr_drillrigtypes_id",                                                 null: false, comment: "Тип буровой установки справочник"
    t.integer  "spr_drillteams_id",                                                    null: false, comment: "Бригада"
    t.index ["spr_diameters_id"], name: "dayreports_diameters_fki", using: :btree
    t.index ["spr_drillrigtypes_id"], name: "dayreports_drillrigtypes_fki", using: :btree
    t.index ["spr_drillteams_id"], name: "dayreports_drillteams_fki", using: :btree
    t.index ["spr_telesystems_id"], name: "dayreports_telesystems_fki", using: :btree
    t.index ["spr_wellpads_id"], name: "dayreport_wellpad_fki", using: :btree
    t.index ["spr_wellpurposes_id"], name: "dayreports_welpurposes_fki", using: :btree
    t.index ["spr_worktypes_id_04"], name: "dayreports_worktypes04_fki", using: :btree
    t.index ["spr_worktypes_id_24"], name: "dayreports_worktypes24_fki", using: :btree
  end

  create_table "spr_bittypes", primary_key: "bittype_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name_",     limit: 100
    t.boolean "is_active",             default: true
  end

  create_table "spr_diameters", primary_key: "diameter_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.integer "diameter",                 null: false
    t.boolean "is_active", default: true, null: false
  end

  create_table "spr_drillrigtypes", primary_key: "drillrigtype_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name_",     limit: 100,                null: false
    t.boolean "is_active",             default: true, null: false
  end

  create_table "spr_drillteams", primary_key: "drillteam_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name_",     limit: 100,                null: false
    t.string  "master",    limit: 100,                null: false
    t.boolean "is_active",             default: true
    t.index ["name_"], name: "drill_teams_name__key", unique: true, using: :btree
  end

  create_table "spr_oilfields", primary_key: "oilfield_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade, comment: "Справочник месторождений" do |t|
    t.string  "fullname",  limit: 100,                null: false
    t.string  "shortname", limit: 10,                 null: false
    t.boolean "is_active",             default: true, null: false
  end

  create_table "spr_requests", primary_key: "request_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name_",     limit: 100
    t.boolean "is_active",             default: true
  end

  create_table "spr_telesystems", primary_key: "telesystem_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name",      limit: 100,                null: false
    t.boolean "is_active",             default: true, null: false
  end

  create_table "spr_turbodrilltypes", primary_key: "turbodrilltype_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name_",     limit: 100
    t.boolean "is_active",             default: true
  end

  create_table "spr_wellpads", primary_key: "wellpad_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade, comment: "Справочник кустовых площадок привязанных к кустам" do |t|
    t.string  "wellpad_number",   limit: 10,                null: false
    t.integer "spr_oilfields_id",                           null: false
    t.boolean "is_active",                   default: true, null: false
    t.index ["spr_oilfields_id"], name: "wellpads_oilfields_fki", using: :btree
  end

  create_table "spr_wellpurposes", primary_key: "WellPurpose_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.string  "FullName",  limit: 100,                null: false
    t.string  "ShortName", limit: 20
    t.boolean "is_active",             default: true
    t.index ["FullName"], name: "WellPurposes_FullName_key", unique: true, using: :btree
  end

  create_table "spr_worktypes", primary_key: "worktype_id", id: :integer, default: -> { "nextval('drill_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name",      limit: 100,                null: false
    t.boolean "is_active",             default: true, null: false
  end

  create_table "tests", primary_key: "test_id", id: :integer, default: -> { "nextval('test_seq'::regclass)" }, force: :cascade do |t|
    t.decimal "number", precision: 6, scale: 2
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "login"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.string   "role",                                     null: false
    t.json     "tokens"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["login"], name: "index_users_on_login", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  add_foreign_key "b_requests", "d_dayreports", column: "b_dayreports_id", primary_key: "dayreport_id", name: "requests_dayreports_fk"
  add_foreign_key "b_requests", "spr_requests", column: "spr_requests_id", primary_key: "request_id", name: "brequests_sprrequest_fk"
  add_foreign_key "d_balance_time", "d_dayreports", column: "d_dayreports_id", primary_key: "dayreport_id", name: "balancetime_dayreport_fk"
  add_foreign_key "d_balance_time", "spr_worktypes", column: "spr_worktypes_id", primary_key: "worktype_id", name: "balancetime_worktype_fk"
  add_foreign_key "d_dayreports", "spr_diameters", column: "spr_diameters_id", primary_key: "diameter_id", name: "dayreports_diameters_fk"
  add_foreign_key "d_dayreports", "spr_drillrigtypes", column: "spr_drillrigtypes_id", primary_key: "drillrigtype_id", name: "dayreports_drillrigtypes_fk"
  add_foreign_key "d_dayreports", "spr_drillteams", column: "spr_drillteams_id", primary_key: "drillteam_id", name: "dayreports_drillteams_fk"
  add_foreign_key "d_dayreports", "spr_telesystems", column: "spr_telesystems_id", primary_key: "telesystem_id", name: "dayreports_telesystems_fk"
  add_foreign_key "d_dayreports", "spr_wellpads", column: "spr_wellpads_id", primary_key: "wellpad_id", name: "dayreport_wellpad_fk"
  add_foreign_key "d_dayreports", "spr_wellpurposes", column: "spr_wellpurposes_id", primary_key: "WellPurpose_id", name: "dayreports_welpurposes_fk"
  add_foreign_key "d_dayreports", "spr_worktypes", column: "spr_worktypes_id_04", primary_key: "worktype_id", name: "dayreports_worktypes04_fk"
  add_foreign_key "d_dayreports", "spr_worktypes", column: "spr_worktypes_id_24", primary_key: "worktype_id", name: "dayreports_worktypes24_fk"
  add_foreign_key "spr_wellpads", "spr_oilfields", column: "spr_oilfields_id", primary_key: "oilfield_id", name: "WELLPADS_OILFIELDS_FK"
end
