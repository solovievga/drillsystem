import { Injectable } from '@angular/core';
import {AuthService} from "../src/app/services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../src/app/shared/functions";

@Injectable()
export class SprDrillrigTypesService {

  constructor(private _authService: AuthService) { }

  comboList(): Observable<any> {
    return this._authService.get('spr_drillrig_types')
      .map(result => result.json().map(record => ({value: record.drillrig_type_id, label: record.name})))
      .catch(handleError)
  }
}
