import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {AuthService} from "../src/app/services/auth/auth.service";
import {handleError} from "../src/app/shared/functions";

@Injectable()
export class SprDrillTeamsService {

  constructor(private _authService: AuthService) { }

  comboList(): Observable<any> {
    return this._authService.get('users')
      .map(result => result.json().map(record => ({value: record.drill_team_id, label: record.name})))
      .catch(handleError)
  }
}
