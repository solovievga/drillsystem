import { TestBed, inject } from '@angular/core/testing';

import { SprWorkTypesService } from './spr-work-types.service';

describe('SprWorkTypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprWorkTypesService]
    });
  });

  it('should ...', inject([SprWorkTypesService], (service: SprWorkTypesService) => {
    expect(service).toBeTruthy();
  }));
});
