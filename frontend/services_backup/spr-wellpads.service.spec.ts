import { TestBed, inject } from '@angular/core/testing';

import { SprWellpadsService } from './spr-wellpads.service';

describe('SprWellpadsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprWellpadsService]
    });
  });

  it('should ...', inject([SprWellpadsService], (service: SprWellpadsService) => {
    expect(service).toBeTruthy();
  }));
});
