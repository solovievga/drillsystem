import { Injectable } from '@angular/core';
import {handleError} from "../src/app/shared/functions";
import {Observable} from "rxjs/Observable";
import {AuthService} from "../src/app/services/auth/auth.service";

@Injectable()
export class SprTelesystemsService {

  constructor(private _authService: AuthService) { }

  comboList(): Observable<any> {
    return this._authService.get('spr_telesystems')
      .map(result => result.json().map(record => ({value: record.telesystem_id, label: record.name})))
      .catch(handleError)
  }
}
