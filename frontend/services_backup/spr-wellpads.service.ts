import { Injectable } from '@angular/core';
import {handleError} from "../src/app/shared/functions";
import {Observable} from "rxjs/Observable";
import {AuthService} from "../src/app/services/auth/auth.service";

@Injectable()
export class SprWellpadsService {

  constructor(private _authService: AuthService) { }

  comboList(): Observable<any> {
    return this._authService.get('spr_wellpads')
      .map(result => result.json().map(record => ({value: record.wellpad_id, label: record.wellpad_number})))
      .catch(handleError)
  }
}
