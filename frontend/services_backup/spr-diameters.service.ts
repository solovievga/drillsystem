import { Injectable } from '@angular/core';
import {AuthService} from "../src/app/services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../src/app/shared/functions";

@Injectable()
export class SprDiametersService {

  constructor(private _authService: AuthService) { }

  comboList(): Observable<any> {
    return this._authService.get('spr_diameters')
      .map(result => result.json().map(record => ({value: record.diameter_id, label: record.diameter})))
      .catch(handleError)
  }
}
