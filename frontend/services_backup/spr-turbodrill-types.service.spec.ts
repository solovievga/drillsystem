import { TestBed, inject } from '@angular/core/testing';

import { SprTurbodrillTypesService } from './spr-turbodrill-types.service';

describe('SprTurbodrillTypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprTurbodrillTypesService]
    });
  });

  it('should ...', inject([SprTurbodrillTypesService], (service: SprTurbodrillTypesService) => {
    expect(service).toBeTruthy();
  }));
});
