import { TestBed, inject } from '@angular/core/testing';

import { SprDiametersService } from './spr-diameters.service';

describe('SprDiametersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprDiametersService]
    });
  });

  it('should ...', inject([SprDiametersService], (service: SprDiametersService) => {
    expect(service).toBeTruthy();
  }));
});
