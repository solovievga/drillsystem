import { TestBed, inject } from '@angular/core/testing';

import { SprDrillrigTypesService } from './spr-drillrig-types.service';

describe('SprDrillrigTypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprDrillrigTypesService]
    });
  });

  it('should ...', inject([SprDrillrigTypesService], (service: SprDrillrigTypesService) => {
    expect(service).toBeTruthy();
  }));
});
