import { Injectable } from '@angular/core';
import {handleError} from "../src/app/shared/functions";
import {Observable} from "rxjs/Observable";
import {AuthService} from "../src/app/services/auth/auth.service";

@Injectable()
export class SprWellPurposesService {

  constructor(private _authService: AuthService) { }

  comboList(): Observable<any> {
    return this._authService.get('spr_well_purposes')
      .map(result => result.json().map(record => ({value: record.well_purpose_id, label: record.short_name || record.full_name})))
      .catch(handleError)
  }
}
