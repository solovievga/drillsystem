import { TestBed, inject } from '@angular/core/testing';

import { SprWellPurposesService } from './spr-well-purposes.service';

describe('SprWellPurposesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprWellPurposesService]
    });
  });

  it('should ...', inject([SprWellPurposesService], (service: SprWellPurposesService) => {
    expect(service).toBeTruthy();
  }));
});
