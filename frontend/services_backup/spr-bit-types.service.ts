import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {AuthService} from "../src/app/services/auth/auth.service";
import {handleError} from "../src/app/shared/functions";

@Injectable()
export class SprBitTypesService {

  constructor(private _authService: AuthService) { }

  comboList(): Observable<any> {
    return this._authService.get('spr_bit_types')
      .map(result => result.json().map(record => ({value: record.bit_type_id, label: record.name})))
      .catch(handleError)
  }
}
