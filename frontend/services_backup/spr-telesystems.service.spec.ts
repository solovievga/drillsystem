import { TestBed, inject } from '@angular/core/testing';

import { SprTelesystemsService } from './spr-telesystems.service';

describe('SprTelesystemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprTelesystemsService]
    });
  });

  it('should ...', inject([SprTelesystemsService], (service: SprTelesystemsService) => {
    expect(service).toBeTruthy();
  }));
});
