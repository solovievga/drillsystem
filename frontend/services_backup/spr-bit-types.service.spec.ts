import { TestBed, inject } from '@angular/core/testing';

import { SprBitTypesService } from './spr-bit-types.service';

describe('SprBitTypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprBitTypesService]
    });
  });

  it('should ...', inject([SprBitTypesService], (service: SprBitTypesService) => {
    expect(service).toBeTruthy();
  }));
});
