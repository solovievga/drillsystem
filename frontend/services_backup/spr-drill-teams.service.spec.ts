import { TestBed, inject } from '@angular/core/testing';

import { SprDrillTeamsService } from './spr-drill-teams.service';

describe('SprDrillTeamsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprDrillTeamsService]
    });
  });

  it('should ...', inject([SprDrillTeamsService], (service: SprDrillTeamsService) => {
    expect(service).toBeTruthy();
  }));
});
