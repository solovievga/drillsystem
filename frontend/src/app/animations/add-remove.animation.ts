import {animate, style, transition, trigger} from "@angular/animations";
export function addRemoveAnimation() {
    return trigger('addRemove', [
        transition(':enter', [
            style({opacity: 0, maxHeight: '0'}),
            animate('0.3s ease-in-out', style({opacity: 1, maxHeight: '100px'}))
        ]),
        transition(':leave', [
            style({opacity: 1, maxHeight: '100px'}),
            animate('0.3s ease-in-out', style({opacity: 0, maxHeight: '0'}))
        ])
    ])
}