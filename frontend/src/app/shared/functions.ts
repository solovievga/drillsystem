import {Observable} from "rxjs/Observable";

declare let moment: any;

export const handleError = (error: any): any => {
  console.error('Произошла ошибка', error);
  return Observable.throw(error.message || error);
};

export const parseDateTime = (dateTime: string): string => {
  return (dateTime) ? moment(dateTime).format("L LT") : ''
};

export const parseDate = (date: string): string => {
  return (date) ? moment(date).format("L") : ''
};

export const parseTime = (time: string): string => {
    return (time) ? moment(time, "HH:mm:ss").format("HH:mm") : ''
};

export const getErrorTextFromJSON = (body: any) => {
    if (body.errors)
        if (body.errors.full_messages)
            return body.errors.full_messages;
        else
            return body.errors[0];
    else {
        return JSON.stringify(body);
    }

};

export const replaceObjectValues = (object: any, oldValue: any, newValue: any) => {
    for (let key in object) {
        if (object[key] === oldValue)
            object[key] = newValue
    }
};