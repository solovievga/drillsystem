import {ToastOptions} from "ng2-toastr";

export class ToastCustomOptions extends ToastOptions {
    animate = 'flyRight';
    positionClass = 'toast-bottom-right';
}