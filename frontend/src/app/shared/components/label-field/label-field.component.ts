import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
    selector: 'label-field',
    templateUrl: './label-field.component.html',
    styleUrls: ['./label-field.component.scss']
})
export class LabelFieldComponent {
    @Input() validatorControl: FormControl;
    @Input() required: boolean = false;

    constructor() {
    }
}
