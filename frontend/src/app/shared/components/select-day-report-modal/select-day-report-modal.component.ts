import { Component, OnInit } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import {DayReport} from "../../../models/day-report.model";
import {getErrorTextFromJSON} from "../../functions";
import {DayReportService} from "../../../services/day-report.service";
import {ToastsManager} from "ng2-toastr";

declare let moment: any;

@Component({
  selector: 'app-select-day-report-modal',
  templateUrl: './select-day-report-modal.component.html',
  styleUrls: ['./select-day-report-modal.component.scss']
})
export class SelectDayReportModalComponent implements CloseGuard, ModalComponent<any> {
  selectedDate: string = moment().format('YYYY-MM-DD');
  loading: boolean = false;
  dayReports: Array<DayReport>;
  selectedDayReport: DayReport;

  constructor(
      public dialog: DialogRef<any>,
      public toastManager: ToastsManager,
      private  _vDayReportService: DayReportService,
  ) {
    this.dayReports = [];
    this.selectedDayReport = new DayReport();
    dialog.setCloseGuard(this);
  }

  onSelectDate(event): void {
    this.loading = true;
    this.selectedDayReport = new DayReport();
    this.selectedDate = event;
    this.loadVDayReports();
  }

  loadVDayReports(): void {
    this._vDayReportService.data(this.selectedDate).subscribe(
        result => {
          this.dayReports = result;
          this.loading = false;
        },
        error => {
          this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка загрузки списка сводки')
        })
  }

  onSelectDayReport(dayReport: DayReport): void {
    this.selectedDayReport = dayReport;
    this.dialog.close(dayReport);
  }

  cancel(): void {
    this.dialog.close(null);
  }

}
