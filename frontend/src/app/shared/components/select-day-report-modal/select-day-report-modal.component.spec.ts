import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectDayReportModalComponent } from './select-day-report-modal.component';

describe('SelectDayReportModalComponent', () => {
  let component: SelectDayReportModalComponent;
  let fixture: ComponentFixture<SelectDayReportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectDayReportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectDayReportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
