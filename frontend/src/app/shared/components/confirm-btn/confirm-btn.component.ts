import {Component, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {Subject} from "rxjs/Subject";

@Component({
  selector: 'confirm-btn',
  templateUrl: './confirm-btn.component.html',
  styleUrls: ['./confirm-btn.component.scss']
})
export class ConfirmBtnComponent {
  confirm: boolean = false;
  done: boolean = false;
  loading: boolean = false;
  doneSubject: Subject<any>;
  private _timeOutHandler: any;
  @Input() icon: boolean = false;
  @Input() disabled: boolean = false;
  @Input() defaultText: string;
  @Input() confirmText: string;
  @Input() loadingText: string;
  @Input() doneText: string;
  @Input() cancelTimeOut: number = 3000;
  @Output() onConfirm: EventEmitter<any> = new EventEmitter();

  constructor(private _el: ElementRef) {
    this.doneSubject = new Subject();
  }

  onClick(event): void {
    if (this.confirm) {
      this.onConfirm.emit();
      this.loading = true;
      this.doneSubject.subscribe((value) => {
        switch (value) {
          case 'done':
            this.done = true;
            this.loading = false;
          case 'error':
            this._resetButton();
        }

      });

    } else {
      this.confirm = true;
    }

    event.stopPropagation();
  }

  @HostListener('mouseover') onMouseOver(): void {
    if (this._timeOutHandler) {
      clearTimeout(this._timeOutHandler);
      this._timeOutHandler = null;
    }
  }

  @HostListener('mouseout') onMouseOut(): void {
    if (this.confirm || this.done) {
      if (!this._timeOutHandler)
        this._timeOutHandler = setTimeout(() => {
          this._resetButton();
        }, this.cancelTimeOut)
    }
  }

  @HostListener('document:click', ['$event']) clickOut(event) {
    this._resetButton();
  }


  private _resetButton(): void {
    this.confirm = false;
    this.done = false;
    this.loading = false;
    this._timeOutHandler = null;
  }

}
