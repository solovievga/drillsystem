import {AfterViewInit, Component, Input} from '@angular/core';
import {FormControl} from "@angular/forms";
import {trigger, style, animate, transition} from '@angular/animations';
import {addRemoveAnimation} from "../../../animations/add-remove.animation";

@Component({
    selector: 'validate-message',
    animations: [addRemoveAnimation()],
    templateUrl: './validate-message.component.html',
    styleUrls: ['./validate-message.component.scss']
})
export class ValidateMessageComponent implements AfterViewInit {
    @Input() control: FormControl;
    isRequired: boolean = false;

    constructor() {
    }

    ngAfterViewInit() {
        if (this.control.errors)
            if (this.control.errors.required) {
                this.isRequired = true;
            }
    }

    get errorMessage(): string {

        if (this.control.errors != null && this.control.enabled)
            for (let propertyName in this.control.errors) {
                if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
                    return this.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
                }
            }

        return null;
    }

    private getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            'required': 'Необходимо заполнить',
            'validateMore': validatorValue.isReverse ? `Поле "${validatorValue.validateSelfName}" должно быть меньше чем поле "${validatorValue.validateFieldName}"` : `Поле "${validatorValue.validateSelfName}" должно быть больше чем поле "${validatorValue.validateFieldName}"`,
            // 'invalidEmailAddress': 'Invalid email address',
            // 'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
            'minlength': `Минимальная длинна ${validatorValue.requiredLength}`
        };

        return config[validatorName];
    }

}
