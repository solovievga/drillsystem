import {Component, Pipe, PipeTransform, Input, OnInit, ViewContainerRef, TemplateRef, ViewChild} from '@angular/core';
import {AbstractControl, ValidatorFn, Validators} from "@angular/forms";
import {DialogRef, ModalComponent} from 'angular2-modal';
import {ComboboxItem} from "../../../models/combobox-item.model";
import {Observable} from 'rxjs/Rx';
import {BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {overlayConfigFactory} from "angular2-modal";
import {getErrorTextFromJSON} from "../../functions";
import {AuthService} from "../../../services/auth/auth.service";
import {ToastsManager} from "ng2-toastr";
import {SprBitType} from "./spr-bit-type/sprBitType";
import {SprChockDiameter} from "./spr-chock-diameters/sprChockDiameter";
import {SprContractSolution} from "./spr-contract-solutions/sprContractSolution";
import {SprDiameter} from "./spr-diameters/sprDiameter";
import {SprDrillrigType} from "./spr-drillrig-types/sprDrillrigType"
import {SprOilfield} from "./spr-oilfields/sprOilfield"
import {SprRequest} from "./spr-requests/sprRequest"
import {SprTelesystem} from "./spr-telesystems/sprTelesystem"
import {SprTurbodrillType} from "./spr-turbodrill-types/sprTurbodrillType"
import {SprWellpad} from "./spr-wellpads/sprWellpad"
import {SprWellPurpose} from "./spr-well-purposes/sprWellPurpose"
import {SprWorkType} from "./spr-work-types/sprWorkType"
import {SprBitTypeService} from "./spr-bit-type/spr-bit-type.service";
import {SprChockDiameterService} from "./spr-chock-diameters/spr-chock-diametrs.service";
import {SprContractSolutionService} from "./spr-contract-solutions/spr-contract-solutions.service";
import {SprDiameterService} from "./spr-diameters/spr-diameters.service";
import {SprDrillrigTypeService} from "./spr-drillrig-types/spr-drillrig-types.service";
import {SprOilfieldService} from "./spr-oilfields/spr-oilfields.service";
import {SprRequestService} from './spr-requests/spr-requests.service';
import {SprTelesystemService} from './spr-telesystems/spr-telesystems.service';
import {SprTurbodrillTypeService} from './spr-turbodrill-types/spr-turbodrill-types.service';
import {SprWellpadService} from './spr-wellpads/spr-wellpads.service';
import {SprWellPurposeService} from './spr-well-purposes/spr-well-purposes.service';
import {SprWorkTypeService} from './spr-work-types/spr-work-types.service';

@Component({
  selector: 'app-open-modal-window',
  templateUrl: './open-modal-window.component.html',
  styleUrls: ['./open-modal-window.component.scss'],
  
  providers: [SprBitTypeService, SprChockDiameterService, SprContractSolutionService, 
    SprDiameterService, SprDrillrigTypeService, SprOilfieldService, 
    SprRequestService, SprTelesystemService, SprTurbodrillTypeService, 
    SprWellpadService, SprWellPurposeService, SprWorkTypeService],
})


export class OpenModalWindowComponent implements OnInit, ModalComponent<any> {
    
        @ViewChild('readOnlyTemplate') readOnlyTemplate: TemplateRef<any>;
        @ViewChild('editTemplate') editTemplate: TemplateRef<any>;
        @ViewChild('readOnlyTemplateSprChockDiameter') readOnlyTemplateSprChockDiameter: TemplateRef<any>;
        @ViewChild('editTemplateSprChockDiameter') editTemplateSprChockDiameter: TemplateRef<any>;
        @ViewChild('readOnlyTemplateSprContractSolution') readOnlyTemplateSprContractSolution: TemplateRef<any>;
        @ViewChild('editTemplateSprContractSolution') editTemplateSprContractSolution: TemplateRef<any>;
        @ViewChild('readOnlyTemplateSprDiameter') readOnlyTemplateSprDiameter: TemplateRef<any>;
        @ViewChild('editTemplateSprDiameter') editTemplateSprDiameter: TemplateRef<any>; 
        @ViewChild('readOnlyTemplateSprDrillrigType') readOnlyTemplateSprDrillrigType: TemplateRef<any>;
        @ViewChild('editTemplateSprDrillrigType') editTemplateSprDrillrigType: TemplateRef<any>;               
        @ViewChild('readOnlyTemplateSprOilfield') readOnlyTemplateSprOilfield: TemplateRef<any>;
        @ViewChild('editTemplateSprOilfield') editTemplateSprOilfield: TemplateRef<any>;  
        @ViewChild('readOnlyTemplateSprRequest') readOnlyTemplateSprRequest: TemplateRef<any>;
        @ViewChild('editTemplateSprRequest') editTemplateSprRequest: TemplateRef<any>; 
        @ViewChild('readOnlyTemplateSprTelesystem') readOnlyTemplateSprTelesystem: TemplateRef<any>;
        @ViewChild('editTemplateSprTelesystem') editTemplateSprTelesystem: TemplateRef<any>; 
        @ViewChild('readOnlyTemplateSprTurbodrillType') readOnlyTemplateSprTurbodrillType: TemplateRef<any>;
        @ViewChild('editTemplateSprTurbodrillType') editTemplateSprTurbodrillType: TemplateRef<any>; 
        @ViewChild('readOnlyTemplateSprWellpad') readOnlyTemplateSprWellpad: TemplateRef<any>;
        @ViewChild('editTemplateSprWellpad') editTemplateSprWellpad: TemplateRef<any>; 
        @ViewChild('readOnlyTemplateSprWellPurpose') readOnlyTemplateSprWellPurpose: TemplateRef<any>;
        @ViewChild('editTemplateSprWellPurpose') editTemplateSprWellPurpose: TemplateRef<any>;
        @ViewChild('readOnlyTemplateSprWorkType') readOnlyTemplateSprWorkType: TemplateRef<any>;
        @ViewChild('editTemplateSprWorkType') editTemplateSprWorkType: TemplateRef<any>;
        sprBitTypes: Array<SprBitType>;
        sprChockDiameters: Array<SprChockDiameter>;
        sprContractSolutions: Array<SprContractSolution>;
        sprDiameters: Array<SprDiameter>;
        sprDrillrigTypes: Array<SprDrillrigType>;
        sprOilfields: Array<SprOilfield>;
        sprRequests: Array<SprRequest>;
        sprTelesystems: Array<SprTelesystem>;
        sprTurbodrillTypes: Array<SprTurbodrillType>;
        sprWellpads: Array<SprWellpad>;
        sprWellPurposes: Array<SprWellPurpose>;
        sprWorkTypes: Array<SprWorkType>;
        isNewRecord: boolean;
        errorMessage: any;
        private isOpenForm: boolean = false;
        private isOpenForm2:boolean = false;
        private isOpenForm3:boolean = false;
        private isOpenForm4: boolean = false;
        private isOpenForm5: boolean = false;
        private isOpenForm6: boolean = false;
        private isOpenForm7: boolean = false;
        private isOpenForm8: boolean = false;
        private isOpenForm9: boolean = false;
        private isOpenForm10: boolean = false;
        private isOpenForm11: boolean = false;
        private isOpenForm12: boolean = false;

  path: string[] = ['sprBitType'];
  order: number = 1; // 1 asc, -1 desc;
  path1: string[] = ['sprChockDiameter'];
  order1: number = 1;
  path2: string[] = ['sprContractSolution'];
  order2: number = 1;
  path3: string[] = ['sprContractSolution'];
  order3: number = 1;
  path4: string[] = ['sprDrillrigType'];
  order4: number = 1;
  path5: string[] = ['sprOilfield'];
  order5: number = 1;
  path6: string[] = ['sprRequest'];
  order6: number = 1;
  path7: string[] = ['sprTelesystem'];
  order7: number = 1;
  path8: string[] = ['sprTurbodrillType'];
  order8: number = 1;
  path9: string[] = ['sprWellPurpuse'];
  order9: number = 1;
  path10: string[] = ['sprWellpad'];
  order10: number = 1;
  path11: string[] = ['sprWorkType'];
  order11: number = 1;


  constructor(      
      public authService: AuthService,
      private serv: SprBitTypeService,
      private servSprChockDiameter: SprChockDiameterService,
      private servSprContractSolution: SprContractSolutionService,
      private servSprDiameter: SprDiameterService,
      private servSprDrillrigType: SprDrillrigTypeService,
      private servSprOilfield: SprOilfieldService,
      private servSprRequest: SprRequestService,
      private servSprTelesystem: SprTelesystemService,
      private servSprTurbodrillType: SprTurbodrillTypeService,
      private servSprWellpad: SprWellpadService,
      private servSprWellPurpose: SprWellPurposeService,
      private servSprWorkType: SprWorkTypeService,
      public dialog: DialogRef<any>,
      public toastManager: ToastsManager,
  ) {
        this.sprBitTypes = new Array<SprBitType>();
         this.sprChockDiameters = new Array<SprChockDiameter>();
         this.sprContractSolutions = new Array<SprContractSolution>();
         this.sprDiameters = new Array<SprDiameter>();
         this.sprDrillrigTypes = new Array<SprDrillrigType>();
         this.sprOilfields = new Array<SprOilfield>();  
         this.sprRequests = new Array<SprRequest>(); 
         this.sprTelesystems = new Array<SprTelesystem>();
         this.sprTurbodrillTypes = new Array<SprTurbodrillType>();
         this.sprWellpads = new Array<SprWellpad>();
         this.sprWellPurposes = new Array<SprWellPurpose>();
         this.sprWorkTypes = new Array<SprWorkType>();
  }

  @Input() sprBitType: SprBitType;
  @Input() sprChockDiameter: SprChockDiameter;
  @Input() sprContractSolution: SprContractSolution;
  @Input() sprDiameter: SprDiameter;
  @Input() sprDrillrigType: SprDrillrigType;
  @Input() sprOilfield: SprOilfield;
  @Input() sprRequest: SprRequest;
  @Input() sprTelesystem: SprTelesystem;
  @Input() sprTurbodrillType: SprTurbodrillType;
  @Input() sprWellpad: SprWellpad;
  @Input() sprWellPurpose: SprWellPurpose;
  @Input() sprWorkType: SprWorkType;


    ngOnInit() {     
        this.loadSprBitTypes();
        this.loadSprChockDiameters();
        this.loadSprContractSolutions();
        this.loadSprDiameter();
        this.loadSprDrillrigType();
        this.loadSprOilfield();
        this.loadSprRequest();
        this.loadSprTelesystem();
        this.loadSprTurbodrillType();
        this.loadSprWellpad();
        this.loadSprWellPurpose();
        this.loadSprWorkType();
    }

// Загрузка данных

    private loadSprBitTypes() { 
        this.serv.getSprBitTypes().subscribe((data: SprBitType[]) => {
            this.sprBitTypes = data;  
        });
    }

    private loadSprChockDiameters() { 
        this.servSprChockDiameter.getSprChockDiameters().subscribe((data: SprChockDiameter[]) => {
                this.sprChockDiameters = data;  
            });
    }

    private loadSprContractSolutions() { 
        this.servSprContractSolution.getSprContractSolutions().subscribe((data: SprContractSolution[]) => {
                this.sprContractSolutions = data;  
            });
    }

    private loadSprDiameter() { 
        this.servSprDiameter.getSprDiameters().subscribe((data: SprDiameter[]) => {
                this.sprDiameters = data;  
            });
    }

    private loadSprDrillrigType() { 
        this.servSprDrillrigType.getSprDrillrigTypes().subscribe((data: SprDrillrigType[]) => {
                this.sprDrillrigTypes = data;  
            });
    }

    private loadSprOilfield() { 
        this.servSprOilfield.getSprOilfields().subscribe((data: SprOilfield[]) => {
                this.sprOilfields = data;  
            });
    }

    private loadSprRequest() { 
        this.servSprRequest.getSprRequests().subscribe((data: SprRequest[]) => {
                this.sprRequests = data;  
            });
    }

    private loadSprTelesystem() { 
        this.servSprTelesystem.getSprTelesystems().subscribe((data: SprTelesystem[]) => {
                this.sprTelesystems = data;  
            });
    }

    private loadSprTurbodrillType() { 
        this.servSprTurbodrillType.getSprTurbodrillTypes().subscribe((data: SprTurbodrillType[]) => {
                this.sprTurbodrillTypes = data;  
            });
    }
    
    private loadSprWellpad() { 
        this.servSprWellpad.getSprWellpads().subscribe((data: SprWellpad[]) => {
                this.sprWellpads = data;  
            });
    }

    private loadSprWellPurpose() { 
        this.servSprWellPurpose.getSprWellPurposes().subscribe((data: SprWellPurpose[]) => {
                this.sprWellPurposes = data;  
            });
    }

    private loadSprWorkType() { 
        this.servSprWorkType.getSprWorkTypes().subscribe((data: SprWorkType[]) => {
                this.sprWorkTypes = data;  
            });
    }

    // Добавление записи

    createPost(sprBitTypes: SprBitType) {
        this.isOpenForm = true;
        this.sprBitType = new SprBitType(sprBitTypes.bit_type_id, sprBitTypes.name, sprBitTypes.is_active, sprBitTypes.rn);
        this.sprBitTypes.unshift(this.sprBitType);
        this.isNewRecord = true;
    }

     createSprChockDiameter(sprChockDiameters: SprChockDiameter) {
        this.isOpenForm2 = true;
        this.sprChockDiameter = new SprChockDiameter(sprChockDiameters.chock_diameter_id, sprChockDiameters.diameter, sprChockDiameters.rn);
        this.sprChockDiameters.unshift(this.sprChockDiameter);
        this.isNewRecord = true;
    }

    createSprContractSolution(sprContractSolutions: SprContractSolution) {
        this.isOpenForm3 = true;
        this.sprContractSolution = new SprContractSolution(sprContractSolutions.contract_solution_id, sprContractSolutions.name);
        this.sprContractSolutions.unshift(this.sprContractSolution);
        this.isNewRecord = true;
    }

    createSprDiameter(sprDiameters: SprDiameter) {
        this.isOpenForm4 = true;
        this.sprDiameter = new SprDiameter(sprDiameters.diameter, sprDiameters.diameter_id, sprDiameters.is_active);
        this.sprDiameters.unshift(this.sprDiameter);
        this.isNewRecord = true;
    }

    createSprDrillrigType(sprDrillrigTypes: SprDrillrigType) {
        this.isOpenForm5 = true;
        this.sprDrillrigType = new SprDrillrigType(sprDrillrigTypes.drillrig_type_id, sprDrillrigTypes.name, sprDrillrigTypes.is_active);
        this.sprDrillrigTypes.unshift(this.sprDrillrigType);
        this.isNewRecord = true;
    }

    createSprOilfield(sprOilfields: SprOilfield) {
        this.isOpenForm6 = true;
        this.sprOilfield = new SprOilfield(sprOilfields.oilfield_id, sprOilfields.full_name, sprOilfields.short_name, sprOilfields.is_active, sprOilfields.distance);
        this.sprOilfields.unshift(this.sprOilfield);
        this.isNewRecord = true;
    }

    createSprRequest(sprRequests: SprRequest) {
        this.isOpenForm7 = true;
        this.sprRequest = new SprRequest(sprRequests.name, sprRequests.is_active, sprRequests.request_id);
        this.sprRequests.unshift(this.sprRequest);
        this.isNewRecord = true;
    }

    createSprTelesystem(sprTelesystems: SprTelesystem) {
        this.isOpenForm8 = true;
        this.sprTelesystem = new SprTelesystem(sprTelesystems.telesystem_id, sprTelesystems.name, sprTelesystems.is_active);
        this.sprTelesystems.unshift(this.sprTelesystem);
        this.isNewRecord = true;
    }

    createSprTurbodrillType(sprTurbodrillTypes: SprTurbodrillType) {
        this.isOpenForm9 = true;
        this.sprTurbodrillType = new SprTurbodrillType(sprTurbodrillTypes.turbodrill_type_id, sprTurbodrillTypes.name, sprTurbodrillTypes.is_active);
        this.sprTurbodrillTypes.unshift(this.sprTurbodrillType);
        this.isNewRecord = true;
    }

    createSprWellPurpose(sprWellPurposes: SprWellPurpose) {
        this.isOpenForm10 = true;
        this.sprWellPurpose = new SprWellPurpose(sprWellPurposes.well_purpose_id, sprWellPurposes.full_name, sprWellPurposes.is_active, sprWellPurposes.short_name);
        this.sprWellPurposes.unshift(this.sprWellPurpose);
        this.isNewRecord = true;
    }

    createSprWellpad(sprWellpads: SprWellpad) {
        this.isOpenForm11 = true;
        this.sprWellpad = new SprWellpad(sprWellpads.wellpad_id, sprWellpads.wellpad_number, sprWellpads.spr_oilfields_id, sprWellpads.is_active);
        this.sprWellpads.unshift(this.sprWellpad);
        this.isNewRecord = true;
    }

    createSprWorkType(sprWorkTypes: SprWorkType) {
        this.isOpenForm12 = true;
        this.sprWorkType = new SprWorkType(sprWorkTypes.work_type_id, sprWorkTypes.name, sprWorkTypes.is_active, sprWorkTypes.full_name, sprWorkTypes.group_name);
        this.sprWorkTypes.unshift(this.sprWorkType);
        this.isNewRecord = true;
    }

    // Редактирование записи

    editSprBitType(sprBitTypes: SprBitType) {
        this.sprBitType = new SprBitType(sprBitTypes.bit_type_id, sprBitTypes.name, sprBitTypes.is_active, sprBitTypes.rn);
    }

    editSprChockDiameter(sprChockDiameters: SprChockDiameter) {
        this.sprChockDiameter = new SprChockDiameter(sprChockDiameters.chock_diameter_id, sprChockDiameters.diameter, sprChockDiameters.rn);
    }

    editSprContractSolution(sprContractSolutions: SprContractSolution) {
        this.sprContractSolution = new SprContractSolution(sprContractSolutions.contract_solution_id, sprContractSolutions.name);
    }
    
    editSprDiameter(sprDiameters: SprDiameter) {
        this.sprDiameter = new SprDiameter(sprDiameters.diameter_id, sprDiameters.diameter, sprDiameters.is_active);
    }

    editSprDrillrigType(sprDrillrigTypes: SprDrillrigType) {
        this.sprDrillrigType = new SprDrillrigType(sprDrillrigTypes.drillrig_type_id, sprDrillrigTypes.name, sprDrillrigTypes.is_active);
    }

    editSprOilfield(sprOilfields: SprOilfield) {
        this.sprOilfield = new SprOilfield(sprOilfields.oilfield_id, sprOilfields.full_name, sprOilfields.short_name, sprOilfields.is_active, sprOilfields.distance);
    }

    editSprRequest(sprRequests: SprRequest) {
        this.sprRequest = new SprRequest(sprRequests.name, sprRequests.is_active, sprRequests.request_id);
    }

    editSprTelesystem(sprTelesystems: SprTelesystem) {
        this.sprTelesystem = new SprTelesystem(sprTelesystems.telesystem_id, sprTelesystems.name, sprTelesystems.is_active);
    }

    editSprTurbodrillType(sprTurbodrillTypes: SprTurbodrillType) {
        this.sprTurbodrillType = new SprTurbodrillType(sprTurbodrillTypes.turbodrill_type_id, sprTurbodrillTypes.name, sprTurbodrillTypes.is_active);
    }

    editSprWellpad(sprWellpads: SprWellpad) {
        this.sprWellpad = new SprWellpad(sprWellpads.wellpad_id, sprWellpads.wellpad_number, sprWellpads.spr_oilfields_id, sprWellpads.is_active);
    }

    editSprWellPurpose(sprWellPurposes: SprWellPurpose) {
        this.sprWellPurpose = new SprWellPurpose(sprWellPurposes.well_purpose_id, sprWellPurposes.full_name, sprWellPurposes.is_active, sprWellPurposes.short_name);
    }

    editSprWorkType(sprWorkTypes: SprWorkType) {
        this.sprWorkType = new SprWorkType(sprWorkTypes.work_type_id, sprWorkTypes.name, sprWorkTypes.is_active, sprWorkTypes.full_name, sprWorkTypes.group_name);
    }


    // Загрузка одного из двух шаблонов

    loadTemplate(sprBitTypes: SprBitType) {
        if (this.sprBitType && this.sprBitType.bit_type_id == sprBitTypes.bit_type_id) {
            return this.editTemplate;
        } else {
            return this.readOnlyTemplate;
        }
    }

    loadTemplateSprChockDiameter(sprChockDiameters: SprChockDiameter) {
        if (this.sprChockDiameter && this.sprChockDiameter.chock_diameter_id == sprChockDiameters.chock_diameter_id) {
            return this.editTemplateSprChockDiameter;
        } else {
            return this.readOnlyTemplateSprChockDiameter;
        }
    }

    loadTemplateSprContractSolution(sprContractSolutions: SprContractSolution) {
        if (this.sprContractSolution && this.sprContractSolution.contract_solution_id == sprContractSolutions.contract_solution_id) {
            return this.editTemplateSprContractSolution;
        } else {
            return this.readOnlyTemplateSprContractSolution;
        }
    }

    loadTemplateSprDiameter(sprDiameters: SprDiameter) {
        if (this.sprDiameter && this.sprDiameter.diameter_id == sprDiameters.diameter_id) {
            return this.editTemplateSprDiameter;
        } else {
            return this.readOnlyTemplateSprDiameter;
        }
    }

    loadTemplateSprDrillrigType(sprDrillrigTypes: SprDrillrigType) {
        if (this.sprDrillrigType && this.sprDrillrigType.drillrig_type_id == sprDrillrigTypes.drillrig_type_id) {
            return this.editTemplateSprDrillrigType;
        } else {
            return this.readOnlyTemplateSprDrillrigType;
        }
    }

    loadTemplateSprOilfield(sprOilfields: SprOilfield) {
        if (this.sprOilfield && this.sprOilfield.oilfield_id == sprOilfields.oilfield_id) {
            return this.editTemplateSprOilfield;
        } else {
            return this.readOnlyTemplateSprOilfield;
        }
    }

    loadTemplateSprRequest(sprRequests: SprRequest) {
        if (this.sprRequest && this.sprRequest.request_id == sprRequests.request_id) {
            return this.editTemplateSprRequest;
        } else {
            return this.readOnlyTemplateSprRequest;
        }
    }

    loadTemplateSprTelesystem(sprTelesystems: SprTelesystem) {
        if (this.sprTelesystem && this.sprTelesystem.telesystem_id == sprTelesystems.telesystem_id) {
            return this.editTemplateSprTelesystem;
        } else {
            return this.readOnlyTemplateSprTelesystem;
        }
    }

    loadTemplateSprTurbodrillType(sprTurbodrillTypes: SprTurbodrillType) {
        if (this.sprTurbodrillType && this.sprTurbodrillType.turbodrill_type_id == sprTurbodrillTypes.turbodrill_type_id) {
            return this.editTemplateSprTurbodrillType;
        } else {
            return this.readOnlyTemplateSprTurbodrillType;
        }
    }

    loadTemplateSprWellpad(sprWellpads: SprWellpad) {
        if (this.sprWellpad && this.sprWellpad.wellpad_id == sprWellpads.wellpad_id) {
            return this.editTemplateSprWellpad;
        } else {
            return this.readOnlyTemplateSprWellpad;
        }
    }

    loadTemplateSprWellPurpose(sprWellPurposes: SprWellPurpose) {
        if (this.sprWellPurpose && this.sprWellPurpose.well_purpose_id == sprWellPurposes.well_purpose_id) {
            return this.editTemplateSprWellPurpose;
        } else {
            return this.readOnlyTemplateSprWellPurpose;
        }
    }

    loadTemplateSprWorkType(sprWorkTypes: SprWorkType) {
        if (this.sprWorkType && this.sprWorkType.work_type_id == sprWorkTypes.work_type_id) {
            return this.editTemplateSprWorkType;
        } else {
            return this.readOnlyTemplateSprWorkType;
        }
    }

    // Сохранение информации

    saveSprBitType() {
        this.isOpenForm = false;
        if (this.isNewRecord) {

            // Добавление записи

            this.serv.createSprBitType(this.sprBitType).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprBitTypes();
            });
        
            this.isNewRecord = false;
            this.sprBitType = null;
        
        } else {
            
            // Изменение записи
           
            this.serv.updateSprBitType(this.sprBitType).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprBitTypes();
            });
            
        this.sprBitType = null;
        }
    }

    saveSprChockDiameter() {
        this.isOpenForm2 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprChockDiameter.createSprChockDiameter(this.sprChockDiameter).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprChockDiameters();
            });
        
            this.isNewRecord = false;
            this.sprChockDiameter = null;
        
        } else {
        
            // Изменение записи
        
            this.servSprChockDiameter.updateSprChockDiameter(this.sprChockDiameter).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprChockDiameters();
            });

        this.sprChockDiameter = null;
        }
    }

    saveSprContractSolution() {
        this.isOpenForm3 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprContractSolution.createSprContractSolution(this.sprContractSolution).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprContractSolutions();
            });

            this.isNewRecord = false;
            this.sprContractSolution = null;

        } else {

            // Изменение записи

            this.servSprContractSolution.updateSprContractSolution(this.sprContractSolution).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprContractSolutions();
            });

        this.sprContractSolution = null;
        }
    }

    saveSprDiameter() {
        this.isOpenForm4 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprDiameter.createSprDiameter(this.sprDiameter).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprDiameter();
            });

            this.isNewRecord = false;
            this.sprDiameter = null;

        } else {

            // Изменение записи

            this.servSprDiameter.updateSprDiameter(this.sprDiameter).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprDiameter();
            });

        this.sprDiameter = null;
        }
    }

    saveSprDrillrigType() {
        this.isOpenForm5 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprDrillrigType.createSprDrillrigType(this.sprDrillrigType).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprDrillrigType();
            });

            this.isNewRecord = false;
            this.sprDrillrigType = null;

        } else {

            // Изменение записи

            this.servSprDrillrigType.updateSprDrillrigType(this.sprDrillrigType).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprDrillrigType();
            });

        this.sprDrillrigType = null;
        }
    }

    saveSprOilfield() {
        this.isOpenForm6 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprOilfield.createSprOilfield(this.sprOilfield).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprOilfield();
            });

            this.isNewRecord = false;
            this.sprOilfield = null;

        } else {

            // Изменение записи

            this.servSprOilfield.updateSprOilfield(this.sprOilfield).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprOilfield();
            });

        this.sprOilfield = null;
        }
    }

    saveSprRequest() {
        this.isOpenForm7 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprRequest.createSprRequest(this.sprRequest).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprRequest();
            });

            this.isNewRecord = false;
            this.sprRequest = null;

        } else {

            // Изменение записи

            this.servSprRequest.updateSprRequest(this.sprRequest).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprRequest();
            });

        this.sprRequest = null;
        }
    }

    saveSprTelesystem() {
        this.isOpenForm8 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprTelesystem.createSprTelesystem(this.sprTelesystem).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprTelesystem();
            });

            this.isNewRecord = false;
            this.sprTelesystem = null;

        } else {

            // Изменение записи

            this.servSprTelesystem.updateSprTelesystem(this.sprTelesystem).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprTelesystem();
            });

        this.sprTelesystem = null;
        }
    }

    saveSprTurbodrillType() {
        this.isOpenForm9 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprTurbodrillType.createSprTurbodrillType(this.sprTurbodrillType).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprTurbodrillType();
            });

            this.isNewRecord = false;
            this.sprTurbodrillType = null;

        } else {

            // Изменение записи

            this.servSprTurbodrillType.updateSprTurbodrillType(this.sprTurbodrillType).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprTurbodrillType();
            });

        this.sprTurbodrillType = null;
        }
    }

    saveSprWellPurpose() {
        this.isOpenForm10 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprWellPurpose.createSprWellPurpose(this.sprWellPurpose).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprWellPurpose();
            });

            this.isNewRecord = false;
            this.sprWellPurpose = null;

        } else {

            // Изменение записи

            this.servSprWellPurpose.updateSprWellPurpose(this.sprWellPurpose).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprWellPurpose();
            });

        this.sprWellPurpose = null;
        }
    }

    saveSprWellpad() {
        this.isOpenForm11 = false;

        if (this.isNewRecord) {
            
            // Добавление записи
        
            this.servSprWellpad.createSprWellpad(this.sprWellpad).subscribe(() => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprWellpad();
            },
            
                error => {
                    this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
                }
        )
        this.isNewRecord = false;
        this.sprWellpad = null;
    }
            // Изменение записи
        else {
            this.servSprWellpad.updateSprWellpad(this.sprWellpad).subscribe(() => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprWellpad();
            },
                error => {
                    this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
                }
            );
            this.sprWellpad = null;
        }
    }

    saveSprWorkType() {
        this.isOpenForm12 = false;

        if (this.isNewRecord) {

            // Добавление записи

            this.servSprWorkType.createSprWorkType(this.sprWorkType).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprWorkType();
            });

            this.isNewRecord = false;
            this.sprWorkType = null;

        } else {

            // Изменение записи

            this.servSprWorkType.updateSprWorkType(this.sprWorkType).subscribe(data => {
                this.toastManager.success('Данные успешно сохранены!', 'Выполнено'),
                this.loadSprWorkType();
            });

        this.sprWorkType = null;
        }
    }


    // отмена редактирования
    cancelSprBitType() {
        this.isOpenForm = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprBitTypes.shift();
            this.isNewRecord = false;
        }
        this.sprBitType = null;
    }

    cancelSprChockDiameter() {
        this.isOpenForm2 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprChockDiameters.shift();
            this.isNewRecord = false;
        }
        this.sprChockDiameter = null;
    }

    cancelSprContractSolution() {
        this.isOpenForm3 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprContractSolutions.shift();
            this.isNewRecord = false;
        }
        this.sprContractSolution = null;
    }

    cancelSprDiameter() {
        this.isOpenForm4 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprDiameters.shift();
            this.isNewRecord = false;
        }
        this.sprDiameter = null;
    }

    cancelSprDrillrigType() {
        this.isOpenForm5 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprDrillrigTypes.shift();
            this.isNewRecord = false;
        }
        this.sprDrillrigType = null;
    }

    cancelSprOilfield() {
        this.isOpenForm6 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprOilfields.shift();
            this.isNewRecord = false;
        }
        this.sprOilfield = null;
    }

    cancelSprRequest() {
        this.isOpenForm7 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprRequests.shift();
            this.isNewRecord = false;
        }
        this.sprRequest = null;
    }

    cancelSprTelesystem() {
        this.isOpenForm8 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprTelesystems.shift();
            this.isNewRecord = false;
        }
        this.sprTelesystem = null;
    }

    cancelSprTurbodrillType() {
        this.isOpenForm9 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprTurbodrillTypes.shift();
            this.isNewRecord = false;
        }
        this.sprTurbodrillType = null;
    }

    cancelSprWellPurpose() {
        this.isOpenForm10 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprWellPurposes.shift();
            this.isNewRecord = false;
        }
        this.sprWellPurpose = null;
    }

    cancelSprWellpad() {
        this.isOpenForm11 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprWellpads.shift();
            this.isNewRecord = false;
        }
        this.sprWellpad = null;
    }

    cancelSprWorkType() {
        this.isOpenForm12 = false;
        // если отмена при добавлении, удаляем последнюю запись
        if (this.isNewRecord) {
            this.sprWorkTypes.shift();
            this.isNewRecord = false;
        }
        this.sprWorkType = null;
    }

    // Удаление записей

    deleteSprBitType(sprBitTypes: SprBitType) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.serv.deleteSprBitType(sprBitTypes.bit_type_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprBitTypes();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprChockDiameter(sprChockDiameters: SprChockDiameter) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprChockDiameter.deleteSprChockDiameter(sprChockDiameters.chock_diameter_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprChockDiameters();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprContractSolution(sprContractSolutions: SprContractSolution) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprContractSolution.deleteSprContractSolution(sprContractSolutions.contract_solution_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprContractSolutions();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprDiameter(sprDiameters: SprDiameter) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprDiameter.deleteSprDiameter(sprDiameters.diameter_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprDiameter();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprDrillrigType(sprDrillrigTypes: SprDrillrigType) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprDrillrigType.deleteSprDrillrigType(sprDrillrigTypes.drillrig_type_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprDrillrigType();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprOilfield(sprOilfields: SprOilfield) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprOilfield.deleteSprOilfield(sprOilfields.oilfield_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprOilfield();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprRequest(sprRequests: SprRequest) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprRequest.deleteSprRequest(sprRequests.request_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprRequest();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprTelesystem(sprTelesystems: SprTelesystem) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprTelesystem.deleteSprTelesystem(sprTelesystems.telesystem_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprTelesystem();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }
    
    deleteSprTurbodrillType(sprTurbodrillTypes: SprTurbodrillType) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprTurbodrillType.deleteSprTurbodrillType(sprTurbodrillTypes.turbodrill_type_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprTurbodrillType();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprWellPurpose(sprWellPurposes: SprWellPurpose) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprWellPurpose.deleteSprWellPurpose(sprWellPurposes.well_purpose_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprWellPurpose();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }
    
    deleteSprWellpad(sprWellpads: SprWellpad) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprWellpad.deleteSprWellpad(sprWellpads.wellpad_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprWellpad();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    deleteSprWorkType(sprWorkType: SprWorkType) {
        if (confirm('Вы уверены, что хотите удалить эту запись?')) {
        this.servSprWorkType.deleteSprWorkType(sprWorkType.work_type_id).subscribe(
            () => {
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
                this.loadSprWorkType();
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    };
        return false;
    }

    // Сортировка       

  sortTable(prop: string) {
    this.path = prop.split('.')
    this.order = this.order * (-1); // change order
    return false; // do not reload
  }

  sortTableChockDiameter(prop: string) {
    this.path1 = prop.split('.')
    this.order1 = this.order1 * (-1); // change order 
    return false; // do not reload
  }

  sortTableContractSolution(prop: string) {
    this.path2 = prop.split('.')
    this.order2 = this.order2 * (-1); // change order
    return false; // do not reload
  }

  sortTableDiameter(prop: string) {
    this.path3 = prop.split('.')
    this.order3 = this.order3 * (-1); // change order
    return false; // do not reload
  }

  sortTableDrillrigType(prop: string) {
    this.path4 = prop.split('.')
    this.order4 = this.order4 * (-1); // change order
    return false; // do not reload
  }

  sortTableOilfield(prop: string) {
    this.path5 = prop.split('.')
    this.order5 = this.order5 * (-1); // change order
    return false; // do not reload
  }
 
  sortTableRequest(prop: string) {
    this.path6 = prop.split('.')
    this.order6 = this.order6 * (-1); // change order
    return false; // do not reload
  }

  sortTableTelesystem(prop: string) {
    this.path7 = prop.split('.')
    this.order7 = this.order7 * (-1); // change order
    return false; // do not reload
  }

  sortTableTurbodrillType(prop: string) {
    this.path8 = prop.split('.')
    this.order8 = this.order8 * (-1); // change order
    return false; // do not reload
  }

  sortTableWellPurpose(prop: string) {
    this.path9 = prop.split('.')
    this.order9 = this.order9 * (-1); // change order
    return false; // do not reload
  }

  sortTableWellpad(prop: string) {
    this.path10 = prop.split('.')
    this.order10 = this.order10 * (-1); // change order
    return false; // do not reload
  }

  sortTableWorkType(prop: string) {
    this.path11 = prop.split('.')
    this.order11 = this.order11 * (-1); // change order
    return false; // do not reload
  }


    cancelModal(): void {
        this.dialog.close(null);
    }

}
