import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprWorkType} from "./sprWorkType";


@Injectable()
export class SprWorkTypeService {

  constructor(private _authService: AuthService) {
  }

  getSprWorkTypes(){
    return this._authService.get('spr_work_types')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprWorkType(record: SprWorkType): Observable<SprWorkType> {
    return this._authService.post('spr_work_types', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprWorkType(record: SprWorkType): Observable<SprWorkType> {
    return this._authService.put(`spr_work_types/${record.work_type_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprWorkType(id: number): Observable<any> {
    return this._authService.delete(`spr_work_types/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

