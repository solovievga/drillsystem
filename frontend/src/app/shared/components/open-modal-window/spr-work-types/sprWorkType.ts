export class SprWorkType{
    constructor(
        public work_type_id: number,
        public name: string,
        public is_active: boolean,
        public full_name: string,
        public group_name: string ) { }
}
