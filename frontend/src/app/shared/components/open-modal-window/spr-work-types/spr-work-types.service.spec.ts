import { TestBed, inject } from '@angular/core/testing';

import { SprWorkTypeService } from './spr-work-types.service';

describe('SprWorkTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprWorkTypeService]
    });
  });

  it('should ...', inject([SprWorkTypeService], (service: SprWorkTypeService) => {
    expect(service).toBeTruthy();
  }));
});
