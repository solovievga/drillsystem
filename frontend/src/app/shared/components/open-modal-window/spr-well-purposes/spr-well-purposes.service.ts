import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprWellPurpose} from "./sprWellPurpose";


@Injectable()
export class SprWellPurposeService {

  constructor(private _authService: AuthService) {
  }

  getSprWellPurposes(){
    return this._authService.get('spr_well_purposes')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprWellPurpose(record: SprWellPurpose): Observable<SprWellPurpose> {
    return this._authService.post('spr_well_purposes', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprWellPurpose(record: SprWellPurpose): Observable<SprWellPurpose> {
    return this._authService.put(`spr_well_purposes/${record.well_purpose_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprWellPurpose(id: number): Observable<any> {
    return this._authService.delete(`spr_well_purposes/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

