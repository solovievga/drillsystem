import { TestBed, inject } from '@angular/core/testing';

import { SprWellPurposeService } from './spr-well-purposes.service';

describe('SprWellPurposeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprWellPurposeService]
    });
  });

  it('should ...', inject([SprWellPurposeService], (service: SprWellPurposeService) => {
    expect(service).toBeTruthy();
  }));
});
