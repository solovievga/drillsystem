export class SprWellPurpose{
    constructor(
        public well_purpose_id  : number,
        public full_name: string,
        public is_active: boolean,
        public short_name: string) { }

}
