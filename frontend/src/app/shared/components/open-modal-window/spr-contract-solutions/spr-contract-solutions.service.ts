import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprContractSolution} from "./sprContractSolution";


@Injectable()
export class SprContractSolutionService {

  constructor(private _authService: AuthService) {
  }

  getSprContractSolutions(){
    return this._authService.get('spr_contract_solutions')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprContractSolution(record: SprContractSolution): Observable<SprContractSolution> {
    return this._authService.post('spr_contract_solutions', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprContractSolution(record: SprContractSolution): Observable<SprContractSolution> {
    return this._authService.put(`spr_contract_solutions/${record.contract_solution_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprContractSolution(id: number): Observable<any> {
    return this._authService.delete(`spr_contract_solutions/${id}`)
      .map(result => null)
      .catch(handleError)
  }
}

