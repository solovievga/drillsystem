import { TestBed, inject } from '@angular/core/testing';

import { SprBitTypeService } from './spr-bit-type.service';

describe('SprBitTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprBitTypeService]
    });
  });

  it('should ...', inject([SprBitTypeService], (service: SprBitTypeService) => {
    expect(service).toBeTruthy();
  }));
});
