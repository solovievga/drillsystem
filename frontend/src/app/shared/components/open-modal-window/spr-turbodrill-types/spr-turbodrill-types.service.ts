import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprTurbodrillType} from "./sprTurbodrillType";


@Injectable()
export class SprTurbodrillTypeService {

  constructor(private _authService: AuthService) {
  }

  getSprTurbodrillTypes(){
    return this._authService.get('spr_turbodrill_types')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprTurbodrillType(record: SprTurbodrillType): Observable<SprTurbodrillType> {
    return this._authService.post('spr_turbodrill_types', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprTurbodrillType(record: SprTurbodrillType): Observable<SprTurbodrillType> {
    return this._authService.put(`spr_turbodrill_types/${record.turbodrill_type_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprTurbodrillType(id: number): Observable<any> {
    return this._authService.delete(`spr_turbodrill_types/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

