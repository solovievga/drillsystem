import { TestBed, inject } from '@angular/core/testing';

import { SprTurbodrillTypeService } from './spr-turbodrill-types.service';

describe('SprTurbodrillTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprTurbodrillTypeService]
    });
  });

  it('should ...', inject([SprTurbodrillTypeService], (service: SprTurbodrillTypeService) => {
    expect(service).toBeTruthy();
  }));
});
