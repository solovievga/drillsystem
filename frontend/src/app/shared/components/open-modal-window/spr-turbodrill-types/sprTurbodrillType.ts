export class SprTurbodrillType{
    constructor(
        public turbodrill_type_id: number,
        public name: string,
        public is_active: boolean) { }

}
