import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprBitType} from "./sprBitType";

@Injectable()
export class SprBitTypeService {

  constructor(private _authService: AuthService) {
  }

  getSprBitTypes(){
    return this._authService.get('spr_bit_types')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprBitType(record: SprBitType): Observable<SprBitType> {
    return this._authService.post('spr_bit_types', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprBitType(record: SprBitType): Observable<SprBitType> {
    return this._authService.put(`spr_bit_types/${record.bit_type_id}`, record)
      .map(result =>result.json()) 
      .catch(handleError)
  }

  deleteSprBitType(id: number): Observable<any> {
    return this._authService.delete(`spr_bit_types/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

