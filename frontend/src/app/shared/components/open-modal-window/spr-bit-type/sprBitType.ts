export class SprBitType{
    constructor(
        public bit_type_id: number,
        public name: string,
        public is_active: boolean,
        public rn: number) { }
}

