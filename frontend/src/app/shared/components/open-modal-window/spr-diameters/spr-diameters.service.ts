import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprDiameter} from "./sprDiameter";


@Injectable()
export class SprDiameterService {

  constructor(private _authService: AuthService) {
  }

  getSprDiameters(){
    return this._authService.get('spr_diameters')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprDiameter(record: SprDiameter): Observable<SprDiameter> {
    return this._authService.post('spr_diameters', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprDiameter(record: SprDiameter): Observable<SprDiameter> {
    return this._authService.put(`spr_diameters/${record.diameter_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprDiameter(id: number): Observable<any> {
    return this._authService.delete(`spr_diameters/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

