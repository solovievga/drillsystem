import { TestBed, inject } from '@angular/core/testing';

import { SprDiameterService } from './spr-diameters.service';

describe('SprDiameterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprDiameterService]
    });
  });

  it('should ...', inject([SprDiameterService], (service: SprDiameterService) => {
    expect(service).toBeTruthy();
  }));
});
