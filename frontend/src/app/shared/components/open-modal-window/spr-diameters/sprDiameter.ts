export class SprDiameter{
    constructor(
        public diameter_id: number,
        public diameter: number,
        public is_active: boolean) { }

}
