import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprDrillrigType} from "./sprDrillrigType";


@Injectable()
export class SprDrillrigTypeService {

  constructor(private _authService: AuthService) {
  }

  getSprDrillrigTypes(){
    return this._authService.get('spr_drillrig_types')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprDrillrigType(record: SprDrillrigType): Observable<SprDrillrigType> {
    return this._authService.post('spr_drillrig_types', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprDrillrigType(record: SprDrillrigType): Observable<SprDrillrigType> {
    return this._authService.put(`spr_drillrig_types/${record.drillrig_type_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprDrillrigType(id: number): Observable<any> {
    return this._authService.delete(`spr_drillrig_types/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

