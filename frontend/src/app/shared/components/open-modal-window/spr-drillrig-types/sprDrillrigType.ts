export class SprDrillrigType{
    constructor(
        public drillrig_type_id: number,
        public name: string,
        public is_active: boolean) { }
}
