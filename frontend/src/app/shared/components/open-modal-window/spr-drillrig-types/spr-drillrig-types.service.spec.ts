import { TestBed, inject } from '@angular/core/testing';

import { SprDrillrigTypeService } from './spr-drillrig-types.service';

describe('SprDrillrigTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprDrillrigTypeService]
    });
  });

  it('should ...', inject([SprDrillrigTypeService], (service: SprDrillrigTypeService) => {
    expect(service).toBeTruthy();
  }));
});


