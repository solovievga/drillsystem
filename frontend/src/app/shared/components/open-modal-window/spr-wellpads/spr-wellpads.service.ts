import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprWellpad} from "./sprWellpad";


@Injectable()
export class SprWellpadService {

  constructor(private _authService: AuthService) {
  }

  getSprWellpads() {
    return this._authService.get('spr_wellpads')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprWellpad(record: SprWellpad): Observable<SprWellpad> {
    return this._authService.post('spr_wellpads', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprWellpad(record: SprWellpad): Observable<SprWellpad> {
    return this._authService.put(`spr_wellpads/${record.wellpad_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprWellpad(id: number): Observable<any> {
    return this._authService.delete(`spr_wellpads/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

