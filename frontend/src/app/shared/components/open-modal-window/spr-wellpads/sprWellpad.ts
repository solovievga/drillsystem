export class SprWellpad{
    constructor(
        public wellpad_id: number,
        public wellpad_number: string,
        public spr_oilfields_id: number,
        public is_active: boolean) { }

}
