import { TestBed, inject } from '@angular/core/testing';

import { SprWellpadService } from './spr-wellpads.service';

describe('SprWellpadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprWellpadService]
    });
  });

  it('should ...', inject([SprWellpadService], (service: SprWellpadService) => {
    expect(service).toBeTruthy();
  }));
});
