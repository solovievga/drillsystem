import { SprWorkType } from '../spr-work-types/sprWorkType';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingWorkType'
})
export class SortingSprWorkTypePipe implements PipeTransform {

  transform(sprWorkTypes: SprWorkType[], path11: string[], order11: number = 1): SprWorkType[] {

    // Check if is not null
    if (!sprWorkTypes || !path11 || !order11) return sprWorkTypes;

    return sprWorkTypes.sort((a: SprWorkType, b: SprWorkType) => {
      // We go for each property followed by path
      path11.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order11 : order11 * (- 1);
    })
  }

}
