import { SprDrillrigType } from '../spr-drillrig-types/sprDrillrigType';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingDrillrigTypes'
})
export class SortingSprDrillrigTypePipe implements PipeTransform {

  transform(sprDrillrigTypes: SprDrillrigType[], path4: string[], order4: number = 1): SprDrillrigType[] {

    // Check if is not null
    if (!sprDrillrigTypes || !path4 || !order4) return sprDrillrigTypes;

    return sprDrillrigTypes.sort((a: SprDrillrigType, b: SprDrillrigType) => {
      // We go for each property followed by path
      path4.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order4 : order4 * (- 1);
    })
  }

}
