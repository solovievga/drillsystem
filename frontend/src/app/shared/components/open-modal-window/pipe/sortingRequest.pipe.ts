import { SprRequest } from '../spr-requests/sprRequest';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingRequest'
})
export class SortingSprRequestPipe implements PipeTransform {

  transform(sprRequests: SprRequest[], path6: string[], order6: number = 1): SprRequest[] {

    // Check if is not null
    if (!sprRequests || !path6 || !order6) return sprRequests;

    return sprRequests.sort((a: SprRequest, b: SprRequest) => {
      // We go for each property followed by path
      path6.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order6 : order6 * (- 1);
    })
  }

}
