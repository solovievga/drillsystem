import { SprWellPurpose } from '../spr-well-purposes/sprWellPurpose';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingWellPurpose'
})
export class SortingSprWellPurposePipe implements PipeTransform {

  transform(sprWellPurposes: SprWellPurpose[], path9: string[], order9: number = 1): SprWellPurpose[] {

    // Check if is not null
    if (!sprWellPurposes || !path9 || !order9) return sprWellPurposes;

    return sprWellPurposes.sort((a: SprWellPurpose, b: SprWellPurpose) => {
      // We go for each property followed by path
      path9.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order9 : order9 * (- 1);
    })
  }

}
