import { SprOilfield } from '../spr-oilfields/sprOilfield';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingOilfields'
})
export class SortingSprOilfieldPipe implements PipeTransform {

  transform(sprOilfields: SprOilfield[], path5: string[], order5: number = 1): SprOilfield[] {

    // Check if is not null
    if (!sprOilfields || !path5 || !order5) return sprOilfields;

    return sprOilfields.sort((a: SprOilfield, b: SprOilfield) => {
      // We go for each property followed by path
      path5.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order5 : order5 * (- 1);
    })
  }

}
