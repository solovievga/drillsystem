import { SprDiameter } from '../spr-diameters/sprDiameter';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingDiameters'
})
export class SortingSprDiameterPipe implements PipeTransform {

  transform(sprDiameters: SprDiameter[], path3: string[], order3: number = 1): SprDiameter[] {

    // Check if is not null
    if (!sprDiameters || !path3 || !order3) return sprDiameters;

    return sprDiameters.sort((a: SprDiameter, b: SprDiameter) => {
      // We go for each property followed by path
      path3.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order3 : order3 * (- 1);
    })
  }

}
