import { SprContractSolution } from '../spr-contract-solutions/sprContractSolution';
import { Pipe, PipeTransform } from '@angular/core';

 @Pipe({
  name: 'sortingSprContractSolutions'
})

export class SortingSprContractSolutionPipe implements PipeTransform {
  transform(sprContractSolutions: SprContractSolution[], path2: string[], order2: number = 1): SprContractSolution[] {
    // Check if is not null
    if (!sprContractSolutions || !path2 || !order2) return sprContractSolutions;
    return sprContractSolutions.sort((a: SprContractSolution, b: SprContractSolution) => {
      // We go for each property followed by path
      path2.forEach(property => {
        a = a[property];
        b = b[property];
      })
      // Order * (-1): We change our order
      return a > b ? order2 : order2 * (- 1);
    })
  }
}
