import { SprBitType } from '../spr-bit-type/sprBitType'; 
import { Pipe, PipeTransform } from '@angular/core'; 

@Pipe({
  name: 'sortingSprBitType'
})

export class SortingSprBitTypePipe implements PipeTransform {
  transform(sprBitTypes: SprBitType[], path: string[], order: number = 1): SprBitType[] {
    // Check if is not null
    if (!sprBitTypes || !path || !order) return sprBitTypes;
    return sprBitTypes.sort((a: SprBitType, b: SprBitType) => {
      // We go for each property followed by path
      path.forEach(property => {
        a = a[property];
        b = b[property];
      })
      // Order * (-1): We change our order
      return a > b ? order : order * (- 1);
    })
  }
}
