import { SprChockDiameter } from '../spr-chock-diameters/sprChockDiameter'; 
import { Pipe, PipeTransform } from '@angular/core'; 

@Pipe({
  name: 'sortingChockDiameters'
})

export class SortingSprChockDiameterPipe implements PipeTransform {

  transform(sprChockDiameters: SprChockDiameter[], path1: string[], order1: number = 1): SprChockDiameter[] {
    // Check if is not null
    if (!sprChockDiameters || !path1 || !order1) return sprChockDiameters;
    return sprChockDiameters.sort((a: SprChockDiameter, b: SprChockDiameter) => {
      // We go for each property followed by path
      path1.forEach(property => {
        a = a[property];
        b = b[property];
      })
      // Order * (-1): We change our order
      return a > b ? order1 : order1 * (- 1);
    })
  }
}
