import { SprTurbodrillType } from '../spr-turbodrill-types/sprTurbodrillType';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingTurbodrillType'
})
export class SortingSprTurbodrillTypePipe implements PipeTransform {

  transform(sprTurbodrillTypes: SprTurbodrillType[], path8: string[], order8: number = 1): SprTurbodrillType[] {

    // Check if is not null
    if (!sprTurbodrillTypes || !path8 || !order8) return sprTurbodrillTypes;

    return sprTurbodrillTypes.sort((a: SprTurbodrillType, b: SprTurbodrillType) => {
      // We go for each property followed by path
      path8.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order8 : order8 * (- 1);
    })
  }

}
