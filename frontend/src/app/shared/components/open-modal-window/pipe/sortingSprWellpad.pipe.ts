import { SprWellpad } from '../spr-wellpads/sprWellpad';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingWellpad'
})
export class SortingSprWellpadPipe implements PipeTransform {

  transform(sprWellpads: SprWellpad[], path10: string[], order10: number = 1): SprWellpad[] {

    // Check if is not null
    if (!sprWellpads || !path10 || !order10) return sprWellpads;

    return sprWellpads.sort((a: SprWellpad, b: SprWellpad) => {
      // We go for each property followed by path
      path10.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order10 : order10 * (- 1);
    })
  }

}
