import { SprTelesystem } from '../spr-telesystems/sprTelesystem';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortingTelesystems'
})
export class SortingSprTelesystemPipe implements PipeTransform {

  transform(sprTelesystems: SprTelesystem[], path7: string[], order7: number = 1): SprTelesystem[] {

    // Check if is not null
    if (!sprTelesystems || !path7 || !order7) return sprTelesystems;

    return sprTelesystems.sort((a: SprTelesystem, b: SprTelesystem) => {
      // We go for each property followed by path
      path7.forEach(property => {
        a = a[property];
        b = b[property];
      })

      // Order * (-1): We change our order
      return a > b ? order7 : order7 * (- 1);
    })
  }

}
