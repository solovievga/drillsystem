import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenModalWindowComponent } from './open-modal-window.component';

describe('OpenModalWindowComponent', () => {
  let component: OpenModalWindowComponent;
  let fixture: ComponentFixture<OpenModalWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenModalWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenModalWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
