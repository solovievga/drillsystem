import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprChockDiameter} from "./sprChockDiameter";
import { Observer } from 'rxjs/Observer';


@Injectable()
export class SprChockDiameterService {
  sprChockDiameters$: Observable<SprChockDiameter[]>;
  private _sprChockDiameterObserver: Observer<SprChockDiameter[]>;
  private _dataStore: {
    sprChockDiameters: SprChockDiameter[];
  };

  constructor(private _authService: AuthService) {
  }

  getSprChockDiameters(){
    return this._authService.get('spr_chock_diameters')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprChockDiameter(record: SprChockDiameter): Observable<SprChockDiameter> {
    return this._authService.post('spr_chock_diameters', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprChockDiameter(record: SprChockDiameter): Observable<SprChockDiameter> {
    return this._authService.put(`spr_chock_diameters/${record.chock_diameter_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprChockDiameter(id: number): Observable<any> {
    return this._authService.delete(`spr_chock_diameters/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}
