export class SprChockDiameter{
    constructor(
        public chock_diameter_id: number,
        public diameter: number,
        public rn: number) { }

}
