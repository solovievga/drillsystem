import { TestBed, inject } from '@angular/core/testing';

import { SprChockDiameterService } from './spr-chock-diametrs.service';

describe('SprChockDiameterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprChockDiameterService]
    });
  });

  it('should ...', inject([SprChockDiameterService], (service: SprChockDiameterService) => {
    expect(service).toBeTruthy();
  }));
});
