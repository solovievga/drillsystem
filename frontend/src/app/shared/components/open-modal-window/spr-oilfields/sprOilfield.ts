export class SprOilfield{
    constructor(
        public oilfield_id: number,
        public full_name: string,
        public short_name: string,
        public is_active: boolean,
        public distance: number) { }
}
