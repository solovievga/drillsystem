import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprOilfield} from "./sprOilfield";


@Injectable()
export class SprOilfieldService {

  constructor(private _authService: AuthService) {
  }

  getSprOilfields(){
    return this._authService.get('spr_oilfields')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprOilfield(record: SprOilfield): Observable<SprOilfield> {
    return this._authService.post('spr_oilfields', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprOilfield(record: SprOilfield): Observable<SprOilfield> {
    return this._authService.put(`spr_oilfields/${record.oilfield_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprOilfield(id: number): Observable<any> {
    return this._authService.delete(`spr_oilfields/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

