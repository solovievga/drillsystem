import { TestBed, inject } from '@angular/core/testing';

import { SprOilfieldService } from './spr-oilfields.service';

describe('SprOilfieldService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprOilfieldService]
    });
  });

  it('should ...', inject([SprOilfieldService], (service: SprOilfieldService) => {
    expect(service).toBeTruthy();
  }));
});
