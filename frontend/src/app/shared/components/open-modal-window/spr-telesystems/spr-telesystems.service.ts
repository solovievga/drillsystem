import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprTelesystem} from "./sprTelesystem";


@Injectable()
export class SprTelesystemService {

  constructor(private _authService: AuthService) {
  }

  getSprTelesystems(){
    return this._authService.get('spr_telesystems')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprTelesystem(record: SprTelesystem): Observable<SprTelesystem> {
    return this._authService.post('spr_telesystems', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprTelesystem(record: SprTelesystem): Observable<SprTelesystem> {
    return this._authService.put(`spr_telesystems/${record.telesystem_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprTelesystem(id: number): Observable<any> {
    return this._authService.delete(`spr_telesystems/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

