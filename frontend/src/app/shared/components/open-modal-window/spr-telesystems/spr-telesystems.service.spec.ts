import { TestBed, inject } from '@angular/core/testing';

import { SprTelesystemService } from './spr-telesystems.service';

describe('SprTelesystemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprTelesystemService]
    });
  });

  it('should ...', inject([SprTelesystemService], (service: SprTelesystemService) => {
    expect(service).toBeTruthy();
  }));
});
