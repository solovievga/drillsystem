import { TestBed, inject } from '@angular/core/testing';

import { SprRequestService } from './spr-requests.service';

describe('SprRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SprRequestService]
    });
  });

  it('should ...', inject([SprRequestService], (service: SprRequestService) => {
    expect(service).toBeTruthy();
  }));
});
