import {Injectable} from '@angular/core';
import {AuthService} from "../../../../services/auth/auth.service";
import {Observable} from "rxjs/Observable";
import {handleError} from "../../../../shared/functions";
import {SprRequest} from "./sprRequest";


@Injectable()
export class SprRequestService {

  constructor(private _authService: AuthService) {
  }

  getSprRequests(){
    return this._authService.get('spr_requests')
      .map(result => result.json())
      .catch(handleError)
  }

  createSprRequest(record: SprRequest): Observable<SprRequest> {
    return this._authService.post('spr_requests', record)
      .map(result => result.json())
      .catch(handleError)
  }

  updateSprRequest(record: SprRequest): Observable<SprRequest> {
    return this._authService.put(`spr_requests/${record.request_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  deleteSprRequest(id: number): Observable<any> {
    return this._authService.delete(`spr_requests/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}

