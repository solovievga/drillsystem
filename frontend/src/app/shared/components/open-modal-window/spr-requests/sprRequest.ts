export class SprRequest{
    constructor(
        public name: string,
        public is_active: boolean,
        public request_id: number) { }

}
