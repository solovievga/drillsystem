import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {LoaderComponent} from './components/loader/loader.component';
import {ConfirmBtnComponent} from './components/confirm-btn/confirm-btn.component';
import {SelectModule} from 'ng-select';
import {ValidateMessageComponent} from './components/validate-message/validate-message.component';
import {AutofocusDirective} from "./directives/autofocus.directive";
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import {ModalModule} from 'angular2-modal';
import {BootstrapModalModule} from 'angular2-modal/plugins/bootstrap';
import {SelectDayReportModalComponent} from "./components/select-day-report-modal/select-day-report-modal.component";
import {OpenModalWindowComponent} from "./components/open-modal-window/open-modal-window.component";
import {NguiDatetimePickerModule, NguiDatetime} from '@ngui/datetime-picker';
import {LabelFieldComponent} from './components/label-field/label-field.component';
import {Ng2PaginationModule} from 'ng2-pagination'; 
import { SortingSprBitTypePipe } from './components/open-modal-window/pipe/sortingSprBitType.pipe';
import {SortingSprChockDiameterPipe} from './components/open-modal-window/pipe/sortingSprChockDiameter.pipe';
import { SortingSprContractSolutionPipe } from './components/open-modal-window/pipe/sortingSprContractSolution.pipe';
import {SortingSprDiameterPipe} from './components/open-modal-window/pipe/sortingSprDiameter.pipe';
import {SortingSprDrillrigTypePipe} from './components/open-modal-window/pipe/sortingSprDrillrigType.pipe';
import {SortingSprOilfieldPipe} from './components/open-modal-window/pipe/sortingSprOilfield.pipe';
import {SortingSprRequestPipe} from './components/open-modal-window/pipe/sortingRequest.pipe';
import {SortingSprTelesystemPipe} from './components/open-modal-window/pipe/sortingSprTelesystem.pipe';
import {SortingSprTurbodrillTypePipe} from './components/open-modal-window/pipe/sortingSprTurbodrillType.pipe';
import {SortingSprWellPurposePipe} from './components/open-modal-window/pipe/sortingSprWellPurpuse.pipe';
import {SortingSprWellpadPipe} from './components/open-modal-window/pipe/sortingSprWellpad.pipe';
import {SortingSprWorkTypePipe} from './components/open-modal-window/pipe/sortingSprWorkType.pipe';


/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

NguiDatetime.locale = {date: 'Дата', year: 'Год', month: 'Месяц', time: 'Время', hour: 'Час', minute: 'Минута'};
NguiDatetime.daysOfWeek = [
    {fullName: 'Понедельник', shortName: 'Пн'},
    {fullName: 'Вторник', shortName: 'Вт'},
    {fullName: 'Среда', shortName: 'Ср'},
    {fullName: 'Четверг', shortName: 'Чт'},
    {fullName: 'Пятница', shortName: 'Пт'},
    {fullName: 'Суббота', shortName: 'Сб'},
    {fullName: 'Воскресенье', shortName: 'Вс'},
];
NguiDatetime.months = [
    {fullName: 'Январь', shortName: 'Янв'},
    {fullName: 'Февраль', shortName: 'Фев'},
    {fullName: 'Март', shortName: 'Мар'},
    {fullName: 'Апрель', shortName: 'Апр'},
    {fullName: 'Май', shortName: 'май'},
    {fullName: 'Июнь', shortName: 'Июн'},
    {fullName: 'Июль', shortName: 'Июл'},
    {fullName: 'Август', shortName: 'Авг'},
    {fullName: 'Сентябрь', shortName: 'Сен'},
    {fullName: 'Октябрь', shortName: 'Окт'},
    {fullName: 'Ноябрь', shortName: 'Ноя'},
    {fullName: 'Декбарь', shortName: 'Дек'},
];

@NgModule({
  imports: [
    CommonModule,
    Ng2PaginationModule,
    RouterModule,
    FormsModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    ReactiveFormsModule,
    SelectModule,
    
    NguiDatetimePickerModule
  ],
    declarations: [ LoaderComponent, SortingSprWellpadPipe,  SortingSprWellPurposePipe, SortingSprWorkTypePipe,  SortingSprOilfieldPipe, SortingSprTurbodrillTypePipe,  SortingSprRequestPipe, SortingSprTelesystemPipe,  SortingSprChockDiameterPipe, SortingSprDrillrigTypePipe, SortingSprDiameterPipe, SortingSprContractSolutionPipe,  ConfirmBtnComponent, SortingSprBitTypePipe,  ValidateMessageComponent, AutofocusDirective, SelectDayReportModalComponent, OpenModalWindowComponent, LabelFieldComponent],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    LoaderComponent,
    ConfirmBtnComponent,
    SelectModule,
    ValidateMessageComponent,
    LabelFieldComponent,
    AutofocusDirective,
    ModalModule,
    BootstrapModalModule,
    SelectDayReportModalComponent,
    OpenModalWindowComponent,
    NguiDatetimePickerModule
  ],
    entryComponents: [SelectDayReportModalComponent, OpenModalWindowComponent]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }
}
