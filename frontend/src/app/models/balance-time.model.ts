export class BalanceTime {
    id: number;
    spr_work_types_id: number;
    time_: number;
    culprit: string;
    num_value: number;
    _destroy: boolean;

    constructor() {
        this._destroy = false;
    }

    serialize(input: any): BalanceTime {
        this.id = input.balance_time_id;
        this.spr_work_types_id = input.spr_work_types_id;
        this.time_ = input.time_;
        this.culprit = input.culprit;
        this.num_value = input.num_value;

        return this;
    }
}