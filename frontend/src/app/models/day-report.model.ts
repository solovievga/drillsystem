import {User} from "./user.model";
import {parseDate, parseDateTime} from "../shared/functions";
import {BalanceTime} from "./balance-time.model";
import {RequestModel} from "./request.model";
import {Chock} from "./chock.model";
export class DayReport {
    day_report_id: number;
    report_day: string;
    spr_wellpads_id: number;
    name: string;
    well: string;
    project_depth: number;
    layer: string;
    spr_well_purposes_id: number;
    spr_diameters_id: number;
    v_absorbing: number;
    drill_start: string;
    drill_start_deviation: number;
    spr_work_types_id_24: number;
    bottom_24: number;
    rest_oil: number;
    rest_aqua: number;
    slotting_number: number;
    slotting_interval: string;
    spr_work_types_id_04: number;
    bottom_04: number;
    sinking_day: number;
    sinking_month: number;
    sinking_year: number;
    spr_telesystems_id: number;
    spr_drillrig_types_id: number;
    users_id: number;
    user: User;
    spr_oilfields_id: number;
    bit_diameter: number;
    spr_bit_types_id: number;
    spr_turbodrill_types_id: number;
    turbodrill_number: number;
    drill_end: string;
    next_angle: number;
    specific_gravity_24: number;
    viscosity_24: number;
    water_separation_24: number;
    percent_greasing_24: number;
    specific_gravity_04: number;
    viscosity_04: number;
    water_separation_04: number;
    percent_greasing_04: number;
    rest_solution: number;
    presence_solution: number;
    balance_times_attributes: Array<BalanceTime>;
    requests_attributes: Array<RequestModel>;
    chocks_attributes: Array<Chock>;
    spr_contract_solutions_id: number;
    bit_is_roller: boolean;
    masters: string;
    bit_number: string;
    bit_work_hours: number;
    drill_speed_prg: number;
    drill_speed_fact: number;
    val24: string;
    val04: string;
    distance: number;

    get displayName(): string {
        return `${this.name} (${this.user.name || '<нет>'})`
    }

    serialize(input: DayReport): DayReport {
        this.day_report_id = input.day_report_id;
        this.report_day = parseDate(input.report_day);
        this.spr_wellpads_id = input.spr_wellpads_id;
        this.name = input.name;
        this.well = input.well;
        this.project_depth = input.project_depth;
        this.layer = input.layer;
        this.spr_well_purposes_id = input.spr_well_purposes_id;
        this.spr_diameters_id = input.spr_diameters_id;
        this.v_absorbing = input.v_absorbing;
        this.drill_start = parseDateTime(input.drill_start);
        this.drill_start_deviation = input.drill_start_deviation;
        this.spr_work_types_id_24 = input.spr_work_types_id_24;
        this.bottom_24 = input.bottom_24;
        this.rest_oil = input.rest_oil;
        this.rest_aqua = input.rest_aqua;
        this.slotting_number = input.slotting_number;
        this.slotting_interval = input.slotting_interval;
        this.spr_work_types_id_04 = input.spr_work_types_id_04;
        this.bottom_04 = input.bottom_04;
        this.sinking_day = input.sinking_day;
        this.sinking_month = input.sinking_month;
        this.sinking_year = input.sinking_year;
        this.spr_telesystems_id = input.spr_telesystems_id;
        this.spr_drillrig_types_id = input.spr_drillrig_types_id;
        this.users_id = input.users_id;
        if (input.user) this.user = input.user;
        this.spr_oilfields_id = input.spr_oilfields_id;
        this.bit_diameter = input.bit_diameter;
        this.spr_bit_types_id = input.spr_bit_types_id;
        this.spr_turbodrill_types_id = input.spr_turbodrill_types_id;
        this.turbodrill_number = input.turbodrill_number;
        this.drill_end = parseDateTime(input.drill_end);
        this.next_angle = input.next_angle;
        this.specific_gravity_24 = input.specific_gravity_24;
        this.viscosity_24 = input.viscosity_24;
        this.water_separation_24 = input.water_separation_24;
        this.percent_greasing_24 = input.percent_greasing_24;
        this.specific_gravity_04 = input.specific_gravity_04;
        this.viscosity_04 = input.viscosity_04;
        this.water_separation_04 = input.water_separation_04;
        this.percent_greasing_04 = input.percent_greasing_04;
        this.rest_solution = input.rest_solution;
        this.presence_solution = input.presence_solution;
        this.spr_contract_solutions_id = input.spr_contract_solutions_id;
        this.bit_is_roller = input.bit_is_roller;
        this.masters = input.masters;
        this.bit_number = input.bit_number;
        this.bit_work_hours = input.bit_work_hours;
        this.drill_speed_prg = input.drill_speed_prg;
        this.drill_speed_fact = input.drill_speed_fact;
        this.val24 = input.val24;
        this.val04 = input.val04;
        this.distance = input.distance;

        if (input.balance_times_attributes)
            this.balance_times_attributes = input.balance_times_attributes.map(r => new BalanceTime().serialize(r));

        if (input.requests_attributes)
            this.requests_attributes = input.requests_attributes.map(r => new RequestModel().serialize(r));

        if (input.chocks_attributes)
            this.chocks_attributes = input.chocks_attributes.map(r => new Chock().serialize(r));

        return this;
    }

}
