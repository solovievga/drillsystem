export class Chock {
    id: number;
    spr_chock_diameters_id: number;
    chock_depth: number;
    _destroy: boolean;

    constructor() {
        this._destroy = false;
    }

    serialize(input: any): Chock {
        this.id = input.chock_id;
        this.spr_chock_diameters_id = input.spr_chock_diameters_id;
        this.chock_depth = input.chock_depth;

        return this;
    }
}