import {parseDateTime} from "../shared/functions";
export class RequestModel {
    id: number;
    spr_requests_id: number;
    date_time: string;
    _destroy: boolean;

    constructor() {
        this._destroy = false;
    }

    serialize(input: any): RequestModel {
        this.id = input.request_id;
        this.spr_requests_id = input.spr_requests_id;
        this.date_time = parseDateTime(input.date_time);

        return this;
    }
}