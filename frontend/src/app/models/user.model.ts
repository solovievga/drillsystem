export class User {
  id: number;
  email: string;
  name: string;
  nickname: string;
  role: string;

  get isManager(): boolean {
      return (this.role == 'manager' || this.role == 'admin')
  }

  get isAdmin(): boolean {
    return this.role == 'admin'
  }

  serialize(input: User): User {
    this.id = input.id;
    this.email = input.email;
    this.name = input.name;
    this.nickname = input.nickname;
    this.role = input.role;

    return this;
  }
}
