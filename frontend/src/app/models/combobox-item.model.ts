export class ComboboxItem {
  value: string;
  label: string;
}
