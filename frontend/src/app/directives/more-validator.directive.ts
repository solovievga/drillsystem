import {Attribute, Directive, forwardRef, Input} from '@angular/core';
import {Validator, AbstractControl, NG_VALIDATORS, FormControl} from '@angular/forms';

@Directive({
    selector: '[validateMore][formControlName]',
    providers: [
        {provide: NG_VALIDATORS, useExisting: forwardRef(() => MoreValidator), multi: true}
    ]
})
export class MoreValidator implements Validator {

    constructor(@Attribute('validateMore') public validateMore: string,
                @Attribute('validateSelfName') public validateSelfName: string,
                @Attribute('validateFieldName') public validateFieldName: string,
                @Attribute('reverse') public reverse: string) {
    }

    private get isReverse() {
        if (!this.reverse) return false;
        return this.reverse === 'true' ? true : false;
    }

    validate(control: FormControl): { [key: string]: any } {
        let value = parseInt(control.value);

        let eControl = control.root.get(this.validateMore);
        let eVal = parseInt(eControl.value);


        if (eControl && value < eVal && !this.isReverse)
            return {
                validateMore: {
                    validateSelfName: this.validateSelfName,
                    validateFieldName: this.validateFieldName,
                    isReverse: this.isReverse
                }
            };

        if (eControl && value > eVal && !this.isReverse) {
            if (eControl.errors) {
                delete eControl.errors['validateMore'];
                if (!Object.keys(eControl.errors).length)
                    eControl.setErrors(null);
            }
        }

        if (eControl && value < eVal && this.isReverse) {
            if (eControl.errors) {
                delete eControl.errors['validateMore'];
                if (!Object.keys(eControl.errors).length)
                    eControl.setErrors(null);
            }
        }

        if (eControl && value > eVal && this.isReverse)
            return {
                validateMore: {
                    validateSelfName: this.validateSelfName,
                    validateFieldName: this.validateFieldName,
                    isReverse: this.isReverse
                }
            };


        return null;
    }
}