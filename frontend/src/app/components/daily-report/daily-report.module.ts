import {NgModule} from '@angular/core';
import {SharedModule} from "../../shared/shared.module";
import {DailyReportComponent} from "./daily-report.component";
import {DailyReportRoutingModule} from "./daily-report-routing.module";
import {DailyReportFormComponent} from './daily-report-form/daily-report-form.component';
import {TextMaskModule} from 'angular2-text-mask';
import {MoreValidator} from "../../directives/more-validator.directive";
import {OrderByPipe} from "../../pipes/order-by.pipe";

@NgModule({
    imports: [DailyReportRoutingModule, SharedModule, TextMaskModule],
    declarations: [DailyReportComponent, DailyReportFormComponent, MoreValidator, OrderByPipe],
  exports: [DailyReportComponent],
  providers: []
})

export class DailyReportModule {

}
