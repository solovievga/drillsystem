import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {DailyReportComponent} from "./daily-report.component";


@NgModule({
  imports: [
    RouterModule.forChild([
      {path: '', component: DailyReportComponent}
    ])
  ],
  exports: [RouterModule]
})
export class DailyReportRoutingModule {
}
