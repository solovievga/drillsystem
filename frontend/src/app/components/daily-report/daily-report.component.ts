import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {AuthService} from "../../services/auth/auth.service";
import {DayReport} from "../../models/day-report.model";
import {DayReportService} from "../../services/day-report.service";
import {DailyReportFormComponent} from "./daily-report-form/daily-report-form.component";
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import {overlayConfigFactory} from "angular2-modal";
import {environment} from "../../../environments/environment";
import {Router} from "@angular/router";
import {ToastsManager} from "ng2-toastr";
import {getErrorTextFromJSON} from "../../shared/functions";
import {OpenModalWindowComponent} from "../../shared/components/open-modal-window/open-modal-window.component";

declare let moment: any;

@Component({
    selector: 'app-daily-report',
    templateUrl: './daily-report.component.html',
    styleUrls: ['./daily-report.component.scss'],
    providers: [DayReportService, Modal]
})
export class DailyReportComponent implements OnInit {
    sidebarIsVisible: boolean = false;
    loading: boolean = false;
    selectedDate: string = moment().format('YYYY-MM-DD');
    dayReports: Array<DayReport>;
    selectedDayReport: DayReport;
    report_url: string = environment.apiUrl + '/reports/svod';
    @ViewChild('dailyReportForm') dailyReportForm: DailyReportFormComponent;

    constructor(public authService: AuthService,
                private  _vDayReportService: DayReportService,
                private _router: Router,
                public toastManager: ToastsManager,
                public modal: Modal,
                vcr: ViewContainerRef) {
        this.dayReports = [];
        this.selectedDayReport = new DayReport();
        toastManager.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.loading = true;
        this.loadVDayReports();
    }

    toggleSidebar() {
        this.sidebarIsVisible = !this.sidebarIsVisible;
    }

    onSelectDayReport(selectedDayReport: DayReport): void {
        this.selectedDayReport = selectedDayReport;
        this.sidebarIsVisible = false;
    }

    onCreate(): void {
        this.selectedDayReport = new DayReport();
        this.selectedDayReport.users_id = this.authService.userData.id;
        this.selectedDayReport.report_day = this.selectedDate;
        this.sidebarIsVisible = false;
    }

    createSelectedItem(): void {
        this.dayReports.push(this.selectedDayReport);
    }

    deleteSelectedItem(): void {
        this.dayReports.splice(this.dayReports.indexOf(this.selectedDayReport), 1);
    }

    onSelectDate(event): void {
        this.loading = true;
        this.selectedDayReport = new DayReport();
        this.selectedDate = event;
        this.loadVDayReports();
    }

    signOut(): void {
        this.authService.signOut().subscribe()
    }

    loadVDayReports(): void {
        this._vDayReportService.data(this.selectedDate).subscribe(
            result => {
                this.dayReports = result;
                this.loading = false;
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка загрузки списка сводки')
            })
    }
onOpenModule() {
        return this.modal.open(OpenModalWindowComponent,  overlayConfigFactory({}, BSModalContext))
}
}
