import {
    ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnInit, Output, Pipe,
    PipeTransform, ViewChild, ViewContainerRef
} from '@angular/core';
import {DayReport} from "../../../models/day-report.model";
import {AbstractControl, Form, FormArray, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {ComboboxItem} from "../../../models/combobox-item.model";
import {DayReportService} from "../../../services/day-report.service";
import {AuthService} from "../../../services/auth/auth.service";
import {ConfirmBtnComponent} from "../../../shared/components/confirm-btn/confirm-btn.component";
import {addRemoveAnimation} from "../../../animations/add-remove.animation";
import {ToastsManager} from "ng2-toastr";
import {getErrorTextFromJSON, replaceObjectValues} from "../../../shared/functions";
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import {overlayConfigFactory} from "angular2-modal";
import {SelectDayReportModalComponent} from "../../../shared/components/select-day-report-modal/select-day-report-modal.component";

export class Test {
    id: number;
    name: string;

    constructor() {

    }

    get fullName(): string {
        return this.id.toString() + this.name;
    }
}

@Component({
    selector: 'daily-report-form',
    animations: [addRemoveAnimation()],
    templateUrl: './daily-report-form.component.html',
    styleUrls: ['./daily-report-form.component.scss'],
    providers: [DayReportService, Modal]
})
export class DailyReportFormComponent implements OnInit, OnChanges {
    @Input() dayReport: DayReport;
    @Input() dayReports: Array<DayReport>;
    loading: boolean = false;
    @Output() vDayReportChange: EventEmitter<DayReport> = new EventEmitter();
    @Output() onDestroy: EventEmitter<any> = new EventEmitter();
    @Output() onCreate: EventEmitter<any> = new EventEmitter();
    form: FormGroup;
    spr_oilfields: Array<ComboboxItem>;
    spr_wellpads: Array<ComboboxItem>;
    spr_wellpads_filtered: Array<ComboboxItem>;
    spr_well_purposes: Array<ComboboxItem>;
    spr_diameters: Array<ComboboxItem>;
    spr_work_types: Array<ComboboxItem>;
    spr_telesystems: Array<ComboboxItem>;
    spr_drillrig_types: Array<ComboboxItem>;
    users: Array<ComboboxItem>;
    spr_bit_types: Array<ComboboxItem>;
    spr_turbodrill_types: Array<ComboboxItem>;
    spr_requests: Array<ComboboxItem>;
    slotting_numbers: Array<ComboboxItem>;
    next_angles: Array<ComboboxItem>;
    spr_contract_solutions: Array<ComboboxItem>;
    spr_chock_diameters: Array<ComboboxItem>;
    @ViewChild('deleteBtn') deleteBtn: ConfirmBtnComponent;

    timeMask = [/[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/];
    dateTimeMask = [/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/, ' ', /[0-2]/, /[0-9]/, ':', /[0-5]/, /[0-9]/];

    get selectDayReports(): Array<ComboboxItem> {
        return this.dayReports.map(dr => ({value: dr.day_report_id.toString(), label: dr.displayName}))
    }

    get solute_24(): string {
        const eVal = 'нет';
        return `${this.form.controls.specific_gravity_24.value || eVal}-${this.form.controls.viscosity_24.value || eVal}-${this.form.controls.water_separation_24.value || eVal}-${this.form.controls.percent_greasing_24.value || eVal}`
    }

    get solute_04(): string {
        const eVal = 'нет';
        return `${this.form.controls.specific_gravity_04.value || eVal}-${this.form.controls.viscosity_04.value || eVal}-${this.form.controls.water_separation_04.value || eVal}-${this.form.controls.percent_greasing_04.value || eVal}`
    }


    constructor(public authService: AuthService,
                private _formBuilder: FormBuilder,
                private _vDayReportService: DayReportService,
                private _cd: ChangeDetectorRef,
                public toastManager: ToastsManager,
                public modal: Modal,
                vcr: ViewContainerRef) {
        this.form = _formBuilder.group({
            day_report_id: [''],
            report_day: [''],
            spr_oilfields_id: ['', Validators.required],
            spr_wellpads_id: [{value: '', disabled: true}, Validators.required],
            well: ['', Validators.required],
            project_depth: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            layer: ['', [Validators.pattern(/[0-9]/)]],
            spr_well_purposes_id: ['', Validators.required],
            spr_diameters_id: ['', Validators.required],
            v_absorbing: ['', Validators.pattern(/[0-9]/)],
            drill_start: ['', Validators.required],
            drill_start_deviation: ['', Validators.required],
            spr_work_types_id_24: ['', Validators.required],
            // solute_24: ['', Validators.required],
            bottom_24: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            rest_oil: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            // rest_aqua: ['', Validators.required],
            slotting_number: ['', [Validators.pattern(/[0-9]/)]],
            slotting_interval: ['', [Validators.pattern(/[0-9]/)]],
            spr_work_types_id_04: ['', Validators.required],
            // solute_04: ['', Validators.required],
            bottom_04: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            sinking_day: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            spr_telesystems_id: [''],
            spr_drillrig_types_id: ['', Validators.required],
            users_id: ['', Validators.required],
            spr_bit_types_id: [''],
            // bit_diameter: ['', Validators.required],
            spr_turbodrill_types_id: [''],
            turbodrill_number: ['', [Validators.pattern(/[0-9]/)]],
            drill_end: ['', Validators.required],
            next_angle: ['', [Validators.pattern(/[0-9]/)]],
            specific_gravity_24: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            viscosity_24: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            water_separation_24: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            percent_greasing_24: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            specific_gravity_04: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            viscosity_04: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            water_separation_04: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            percent_greasing_04: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            rest_solution: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            presence_solution: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
            balance_times_attributes: _formBuilder.array([]),
            requests_attributes: _formBuilder.array([]),
            chocks_attributes: _formBuilder.array([]),
            spr_contract_solutions_id: ['', Validators.required],
            bit_is_roller: [''],
            masters: [''],
            bit_number: [''],
            bit_work_hours: [''],
            drill_speed_prg: [''],
            drill_speed_fact: [''],
            val24: [''],
            val04: ['']
        });
        // this.onAddBalanceTime();

        toastManager.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.form.controls.spr_oilfields_id.valueChanges.subscribe(changes => {
            if (changes)
                this.form.controls.spr_wellpads_id.enable();
            else
                this.form.controls.spr_wellpads_id.disable();

            if (this.spr_wellpads) {
                this.spr_wellpads_filtered = this.spr_wellpads.filter(r => r['spr_oilfields_id'] == changes);
                this._cd.detectChanges();
            }
        });

        this.loading = true;
        this._vDayReportService.references().subscribe(
            result => {
                this.loading = false;
                this.spr_oilfields = result.spr_oilfields.map(r => ({
                    value: r.oilfield_id.toString(),
                    label: r.short_name || r.full_name,
                    disabled: !r.is_active
                }));
                this.spr_wellpads = result.spr_wellpads.map(r => ({
                    value: r.wellpad_id.toString(),
                    label: r.wellpad_number,
                    spr_oilfields_id: r.spr_oilfields_id,
                    disabled: !r.is_active
                }));
                this.spr_wellpads_filtered = this.spr_wellpads;
                this.spr_well_purposes = result.spr_well_purposes.map(r => ({
                    value: r.well_purpose_id.toString(),
                    label: r.short_name || r.full_name,
                    disabled: !r.is_active
                }));
                this.spr_diameters = result.spr_diameters.map(r => ({
                    value: r.diameter_id.toString(),
                    label: r.diameter,
                    disabled: !r.is_active
                }));
                this.spr_work_types = result.spr_work_types.map(r => ({
                    value: r.work_type_id.toString(),
                    label: r.name,
                    disabled: !r.is_active
                }));
                this.spr_telesystems = result.spr_telesystems.map(r => ({
                    value: r.telesystem_id.toString(),
                    label: r.name,
                    disabled: !r.is_active
                }));
                this.spr_drillrig_types = result.spr_drillrig_types.map(r => ({
                    value: r.drillrig_type_id.toString(),
                    label: r.name,
                    disabled: !r.is_active
                }));
                this.users = result.users.map(r => ({value: r.id.toString(), label: r.name}));
                this.spr_bit_types = result.spr_bit_types.map(r => ({value: r.bit_type_id.toString(), label: r.name, disabled: !r.is_active}));
                this.spr_turbodrill_types = result.spr_turbodrill_types.map(r => ({
                    value: r.turbodrill_type_id.toString(),
                    label: r.name,
                    disabled: !r.is_active
                }));
                this.spr_requests = result.spr_requests.map(r => ({value: r.request_id.toString(), label: r.name, disabled: !r.is_active}));
                this.spr_contract_solutions = result.spr_contract_solutions.map(r => ({
                    value: r.contract_solution_id.toString(),
                    label: r.name,
                }));
                this.spr_chock_diameters = result.spr_chock_diameters.map(r => ({
                    value: r.chock_diameter_id.toString(),
                    label: r.diameter
                }));
            },
            error => {
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка загрузки справочников')
            });

        this.slotting_numbers = [];
        for (let i = 0; i < 20; i++) {
            let val = i + 1;
            this.slotting_numbers[i] = {value: val.toString(), label: val.toString()};
        }

        this.next_angles = [];
        let next_angle_val = 1.00;
        for (let i = 0; i < 60; i++) {
            next_angle_val += 0.01;
            this.next_angles[i] = {value: next_angle_val.toFixed(2), label: next_angle_val.toFixed(2)};
        }
    }

    ngOnChanges(changes) {
        if (changes.dayReport) {
            this.resetData(changes.dayReport.currentValue);
        }
    }

    resetData(newData: DayReport): void {
        let resetData: any = {};
        for (let key in this.form.controls) {
            if (newData[key] != undefined)
                resetData[key] = newData[key].toString();
        }

        this.form.reset(resetData);

        this.resetBalanceTimes(newData);
        this.resetRequests(newData);
        this.resetChocks(newData);

        this.form.controls.spr_oilfields_id.valueChanges.subscribe(changes => {
            this.form.controls.spr_wellpads_id.setValue(resetData.spr_wellpads_id)
        });

        if (this.authService.userData.isManager)
            this.form.controls.users_id.enable();
        else
            this.form.controls.users_id.disable();
    }

    onSave(): void {
        let data = this.form.value;
        replaceObjectValues(data, undefined, '');
        console.log('formData', data)
        if (this.dayReport.day_report_id) {
            this._vDayReportService.update(data).subscribe(
                result => {
                    this.dayReport.serialize(result);
                    this.resetData(this.dayReport);
                    this.vDayReportChange.emit(this.dayReport);
                    this.toastManager.success('Данные успешно сохранены!', 'Выполнено')
                },
                error => {
                    this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
                }
            )
        } else {
            this._vDayReportService.create(data).subscribe(
                result => {
                    this.dayReport.serialize(result);
                    this.resetData(this.dayReport);
                    this.vDayReportChange.emit(this.dayReport);
                    this.onCreate.emit();
                    this.toastManager.success('Данные успешно сохранены!', 'Выполнено')
                },
                error => {
                    this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
                }
            );
        }
    }

    onDelete(): void {
        this._vDayReportService.destroy(this.dayReport.day_report_id).subscribe(
            () => {
                this.deleteBtn.doneSubject.next('done');
                let emptyData: any = {};
                this.dayReport.serialize(emptyData);
                this.form.reset(emptyData);
                this.vDayReportChange.emit(this.dayReport);
                this.onDestroy.emit();
                this.toastManager.success('Данные успешно удалены!', 'Выполнено')
            },
            error => {
                this.deleteBtn.doneSubject.next('error');
                this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
            }
        )
    }

    get balanceTimesControls(): AbstractControl[] {
        let balanceTimes = <FormArray>this.form.get('balance_times_attributes');
        return balanceTimes.controls
    }

    get requestsControls(): AbstractControl[] {
        let requests = <FormArray>this.form.get('requests_attributes');
        return requests.controls
    }

    get chocksControls(): AbstractControl[] {
        let chocks = <FormArray>this.form.get('chocks_attributes');
        return chocks.controls
    }

    onAddBalanceTime(): void {
        let balanceTimes = <FormArray>this.form.controls['balance_times_attributes'];
        balanceTimes.push(this.initBalanceTime())
    }

    initBalanceTime(data: any = {}): FormGroup {
        return this._formBuilder.group({
            id: [data.id],
            spr_work_types_id: [(data.spr_work_types_id) ? data.spr_work_types_id.toString() : '', Validators.required],
            time_: [data.time_, [Validators.required, Validators.pattern(/[0-9]/)]],
            culprit: [data.culprit],
            num_value: [data.num_value, [Validators.required]],
            _destroy: [false]
        })
    }

    onDeleteFromArray(form: FormGroup): void {
        form.controls._destroy.setValue(true);
        for (let key in form.controls) {
            if (form.controls[key].validator) {
                form.controls[key].clearValidators();
                form.controls[key].updateValueAndValidity('');
            }
        }
    }

    clearFormArray(key: string): void {
        let array = <FormArray>this.form.controls[key];
        while (array.length)
            array.removeAt(0);
    }

    resetBalanceTimes(newData: any) {
        this.clearFormArray('balance_times_attributes');
        if (newData.balance_times_attributes) {
            let balanceTimes = <FormArray>this.form.controls['balance_times_attributes'];
            newData.balance_times_attributes.forEach((balance_time) => {
                balanceTimes.push(this.initBalanceTime(balance_time))
            })
        }
    }

    onAddRequest(): void {
        let requests = <FormArray>this.form.controls['requests_attributes'];
        requests.push(this.initRequest())
    }

    initRequest(data: any = {}): FormGroup {
        return this._formBuilder.group({
            id: [data.id],
            spr_requests_id: [(data.spr_requests_id) ? data.spr_requests_id.toString() : '', Validators.required],
            date_time: [data.date_time, [Validators.required]],
            _destroy: [false]
        })
    }

    resetRequests(newData: any) {
        this.clearFormArray('requests_attributes');
        if (newData.requests_attributes) {
            let requests = <FormArray>this.form.controls['requests_attributes'];
            newData.requests_attributes.forEach((request) => {
                requests.push(this.initRequest(request))
            })
        }
    }

    onAddChock(): void {
        let requests = <FormArray>this.form.controls['chocks_attributes'];
        requests.push(this.initChock())
    }

    initChock(data: any = {}): FormGroup {
        return this._formBuilder.group({
            id: [data.id],
            spr_chock_diameters_id: [(data.spr_chock_diameters_id) ? data.spr_chock_diameters_id.toString() : '', Validators.required],
            chock_depth: [data.chock_depth, [Validators.required]],
            _destroy: [false]
        })
    }

    resetChocks(newData: any) {
        this.clearFormArray('chocks_attributes');
        if (newData.chocks_attributes) {
            let chocks = <FormArray>this.form.controls['chocks_attributes'];
            newData.chocks_attributes.forEach((chock) => {
                chocks.push(this.initChock(chock))
            })
        }
    }

    test() {
        console.log(this.form);
        let a: Test;
        console.log(a)
    }

    onPasteData() {
        return this.modal.open(SelectDayReportModalComponent,  overlayConfigFactory({}, BSModalContext)).then(dialog => {
            dialog.result.then(dayReport => {
                if (dayReport) {
                    let currentFormData = this.form.value;

                    delete dayReport.day_report_id;
                    delete dayReport.report_day;
                    delete dayReport.balance_times_attributes;
                    delete dayReport.requests_attributes;
                    delete dayReport.chocks_attributes;

                    Object.assign(currentFormData, dayReport);

                    this.resetData(currentFormData)
                }
            });
        });
    }

}
