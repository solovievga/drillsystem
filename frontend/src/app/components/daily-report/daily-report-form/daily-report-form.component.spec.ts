import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyReportFormComponent } from './daily-report-form.component';

describe('DailyReportFormComponent', () => {
  let component: DailyReportFormComponent;
  let fixture: ComponentFixture<DailyReportFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyReportFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyReportFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
