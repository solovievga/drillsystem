import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {getErrorTextFromJSON} from "../../shared/functions";
import {ToastsManager} from "ng2-toastr";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  loading: boolean = false;
  form: FormGroup;

  constructor(private _router: Router,
              private _authService: AuthService,
              private _formBuilder: FormBuilder,
              public toastManager: ToastsManager,
              vcr: ViewContainerRef) {
    toastManager.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.form = this._formBuilder.group({
      login: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(4)]]
    })
  }

  signIn(): void {
    this.loading = true;
    this._authService.signIn(this.form.value).subscribe(
      () => {
        this.loading = false;
        this._router.navigate(['daily_report']);
      },
      (error) => {
        this.loading = false;
        this.toastManager.error(getErrorTextFromJSON(error.json()), 'Ошибка')
      }
    )
  }

}
