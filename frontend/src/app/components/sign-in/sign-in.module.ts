import {NgModule} from '@angular/core';
import {SharedModule} from "../../shared/shared.module";
import {SignInRoutingModule} from "./sign-in-routing.module";
import {SignInComponent} from "./sign-in.component";


@NgModule({
  imports: [SignInRoutingModule, SharedModule],
  declarations: [SignInComponent],
  exports: [SignInComponent],
  providers: []
})
export class SignInModule {
}
