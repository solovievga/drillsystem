import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {RoutingModule} from "./routings/routing.module";
import {SharedModule} from "./shared/shared.module";
import {environment} from "../environments/environment";
import {AuthService} from "./services/auth/auth.service";
import {User} from "./models/user.model";
import {SprChockDiameterService} from "./shared/components/open-modal-window/spr-chock-diameters/spr-chock-diametrs.service";
import {SprChockDiameter} from "./shared/components/open-modal-window/spr-chock-diameters/sprChockDiameter";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastModule, ToastOptions} from "ng2-toastr";
import {ToastCustomOptions} from "./shared/toast-cutom-options";
import {SelectDayReportModalComponent} from "./shared/components/select-day-report-modal/select-day-report-modal.component";
import {OpenModalWindowComponent} from "./shared/components/open-modal-window/open-modal-window.component";
import {DayReportService} from "./services/day-report.service";
import {Ng2PaginationModule} from 'ng2-pagination';

declare let moment: any;

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    Ng2PaginationModule,
    RoutingModule,
    ToastModule.forRoot()
  ],
  providers: [AuthService, DayReportService, SprChockDiameterService,  {provide: ToastOptions, useClass: ToastCustomOptions}],
    bootstrap: [AppComponent]
})
export class AppModule {

  constructor(public authService: AuthService) {
    moment.locale('ru');

    this.authService.init({
      apiPath: environment.apiUrl,
      signInRedirect: 'sign_in',
      signInPath: '/sign_in',
      validateTokenFailedRedirect: 'sign_in',
      signOutFailedValidate: true,
      dataStorage: localStorage,
      userDataClass: User
    });
  }
}
