import {NgModule} from '@angular/core';
import {Routes, RouterModule} from "@angular/router";
import {AuthService} from "../services/auth/auth.service";

const routes: Routes = [
  {path: '', redirectTo: 'daily_report', pathMatch: 'full'},
  {path: 'sign_in', loadChildren: '../components/sign-in/sign-in.module#SignInModule'},
  {path: 'daily_report', loadChildren: '../components/daily-report/daily-report.module#DailyReportModule', canLoad: [AuthService]}
  // {path: 'sign_in', loadChildren: '../components/user/authorize/authorize.module#AuthorizeModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class RoutingModule {

}
