export interface LogInData {
  email: string;
  password: string;
}

export interface SignUpData {
  email: string;
  password: string;
  passwordConfirmation: string;
}

export interface GlobalOptions {
  headers?: {[key: string]: string;}
}

export interface AuthOptions {
  apiPath?: string;
  apiSignInPath?: string;
  signInPath?: string;
  signInRedirect?: string;
  dataStorage?: any;
  apiSignOutPath?: string;
  validateTokenFailedRedirect?: string;
  signOutFailedValidate?: boolean;
  signInStoredUrlStorageKey?: string,

  apiValidateTokenPath?: string;

  apiDeleteAccountPath?: string;
  apiRegisterAccountPath?: string;
  registerAccountCallbackPath?: string;

  apiUpdatePasswordPath?: string;
  apiResetPasswordPath?: string;
  resetPasswordCallbackPath?: string;

  oAuthBase?: string;
  oAuthPaths?: {[key: string]: string;};
  oAuthCallbackPath?: string;
  oAuthWindowType?: string;
  oAuthWindowOptions?: {[key: string]: string;};

  globalOptions?: GlobalOptions;

  authDataChecking?: boolean;
  authDataCheckingInterval?: number;

  userDataClass: any;
}

export interface AuthData {
  accessToken: string;
  client: string;
  expiry: string;
  tokenType: string;
  uid: string;
}

export interface UserData {
  id: number;
  provider: string;
  uid: string;
  name: string;
  nickname: string;
  image: any;
  email: string;
}

export interface RegisterData {
  [key: string]: string;
}

export interface SignInData {
  login: string;
  email: string;
  password: string;
  userType?: string;
}

export interface UpdatePasswordData {
  password: string;
  passwordConfirmation: string;
  passwordCurrent: string;
  userType?: string;
  resetPasswordToken?: string;
}

export interface ResetPasswordData {
  email: string;
  userType?: string;
}
