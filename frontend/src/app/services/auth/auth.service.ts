import {Injectable, Optional} from "@angular/core";
import {CanActivate, CanLoad, Router, ActivatedRoute} from "@angular/router";
import {
  AuthOptions, AuthData, UserData, RegisterData, SignInData, UpdatePasswordData,
  ResetPasswordData
} from "./auth.model";
import {Http, Response, RequestOptionsArgs, RequestMethod, RequestOptions, Headers, Request} from "@angular/http";
import {Observable, Subject} from "rxjs";


@Injectable()
export class AuthService implements CanActivate, CanLoad {

  private _authOptions: AuthOptions;
  private _authData: AuthData;
  private _userData: any;
  private _authDataInterval: any;

  constructor(private _http: Http,
              @Optional() private _activatedRoute: ActivatedRoute,
              @Optional() private _router: Router) {
  }

  //--------------------------------------------Public Methods----------------------------------------------------------

  canActivate(): boolean {
    if (this.userSignedIn())
      return true;
    else {
      //Запись текущего URL в хранилище
      if (this._authOptions.signInStoredUrlStorageKey) {
        this._authOptions.dataStorage.setItem(
          this._authOptions.signInStoredUrlStorageKey,
          window.location.pathname + window.location.search
        );
      }

      //Переадресация пользователя на signInStoredUrlStorageKey если он установлен
      if (this._router && this._authOptions.signInRedirect)
        this._router.navigate([this._authOptions.signInRedirect]);

      return false;
    }
  }

  canLoad(): boolean {
    return this.canActivate()
  }

  get userData(): any {
    if (this._userData)
      return this._userData;
    else
      return {
        id: 0,
        provider: '',
        uid: '',
        name: '',
        nickname: '',
        image: '',
        email: ''
      }
  }

  get waitUserData(): Observable<boolean> {
    return Observable.create(observer => {
      if (this._userData) {
        observer._next(true);
        observer.complete();
      } else {
        if (this.userSignedIn()) {
          let interval = setInterval(() => {
            if (this._userData) {
              clearInterval(interval);
              observer._next(true);
              observer.complete();
            }
          }, 100)
        } else {
          observer.complete();
        }
      }
    });
  }

  get authData(): AuthData {
    return this._authData;
  }


  //Инициализация сервиса
  init(options?: AuthOptions) {
    let defaultAuthOptions: AuthOptions = {
      apiPath: null,
      apiSignInPath: 'auth/sign_in',
      signInPath: '',
      signInRedirect: null,
      signInStoredUrlStorageKey: null,
      dataStorage: localStorage,
      apiSignOutPath: 'auth/sign_out',
      signOutFailedValidate: false,

      apiValidateTokenPath: 'auth/validate_token',

      apiDeleteAccountPath: 'auth',
      apiRegisterAccountPath: 'auth',
      registerAccountCallbackPath: window.location.href,

      apiUpdatePasswordPath: 'auth',
      apiResetPasswordPath: 'auth/password',
      resetPasswordCallbackPath: window.location.href,

      oAuthBase: window.location.origin,
      oAuthPaths: {
        github: 'auth/github'
      },

      oAuthCallbackPath: 'auth_callback',
      oAuthWindowType: 'newWindow',
      oAuthWindowOptions: null,

      globalOptions: {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      },
      authDataChecking: false,
      authDataCheckingInterval: 5000,
      userDataClass: null
    };

    this._authOptions = (<any>Object).assign(defaultAuthOptions, options);

    this._loadAuthData();
    this._processAuthDataChecking();
  }

  userSignedIn(): boolean {
    return !!this._authData;
  }

  //Регистрация аккаута
  registerAccount(registerData: RegisterData): Observable<Response> {
    registerData['password_confirmation'] = registerData['passwordConfirmation'];
    delete registerData['passwordConfirmation'];

    registerData['confirm_success_url'] = this._authOptions.registerAccountCallbackPath;

    return this.post(this._authOptions.apiRegisterAccountPath, JSON.stringify(registerData));
  }

  //Удаление аккаунта
  deleteAccount(): Observable<Response> {
    return this.delete(this._authOptions.apiDeleteAccountPath);
  }

  //Авторизация и установка значений хранилища
  signIn(signInData: SignInData): Observable<Response> {
    let body = JSON.stringify({
      login: signInData.login,
      email: signInData.email,
      password: signInData.password
    });

    let observ = this.post(this._authOptions.apiSignInPath, body);
    observ.subscribe(res => this._userData = new this._authOptions.userDataClass().serialize(res.json().data), _error => null);

    return observ;
  }

  signInOAuth(oAuthType: string) {
    let oAuthPath: string = this._getOAuthPath(oAuthType);
    let callbackUrl: string = `${window.location.origin}/${this._authOptions.oAuthCallbackPath}`;
    let oAuthWindowType: string = this._authOptions.oAuthWindowType;
    let authUrl: string = this._getOAuthUrl(oAuthPath, callbackUrl, oAuthWindowType);

    if (oAuthWindowType == 'newWindow') {
      let oAuthWindowOptions = this._authOptions.oAuthWindowOptions;
      let windowOptions = '';

      if (oAuthWindowOptions) {
        for (let key in oAuthWindowOptions) {
          windowOptions += `,${key}=${oAuthWindowOptions[key]}`;
        }
      }

      let popup = window.open(
        authUrl,
        '_blank',
        `closebuttoncaption=Cancel${windowOptions}`
      );

      return this._requestCredentialsViaPostMessage(popup);
    } else if (oAuthWindowType == 'sameWindow') {
      window.location.href = authUrl;
    } else {
      throw `Unsupported oAuthWindowType "${oAuthWindowType}"`;
    }
  }

  loadParams(): void {
    this._getAuthDataFromParams();
  }

  //Запрос на выход и очистка хранилища
  signOut(): Observable<Response> {
    let observ = this.delete(this._authOptions.apiSignOutPath);

    this._clearAuthData();

    console.log('sign out', this._authOptions.validateTokenFailedRedirect)
    if (this._authOptions.validateTokenFailedRedirect)
      this._router.navigate([this._authOptions.validateTokenFailedRedirect]);

    return observ;
  }

  //Валидация токена на сервере
  validateToken(): Observable<Response> {
    let observ = this.get(this._authOptions.apiValidateTokenPath);

    observ.subscribe(
      res => {
        this._userData = new this._authOptions.userDataClass().serialize(res.json().data);
        if (this._router.url == this._authOptions.signInPath) {
          this._router.navigate(['/'])
        }
      },
      error => {
        if (error.status === 401 && this._authOptions.signOutFailedValidate) {
          this.signOut();
        }
      });

    return observ;
  }

  //Обновление пароля
  updatePassword(updatePasswordData: UpdatePasswordData): Observable<Response> {
    let args: any;

    if (updatePasswordData.passwordCurrent == null) {
      args = {
        password: updatePasswordData.password,
        password_confirmation: updatePasswordData.passwordConfirmation
      }
    } else {
      args = {
        current_password: updatePasswordData.passwordCurrent,
        password: updatePasswordData.password,
        password_confirmation: updatePasswordData.passwordConfirmation
      };
    }

    if (updatePasswordData.resetPasswordToken) {
      args.reset_password_token = updatePasswordData.resetPasswordToken;
    }

    let body = JSON.stringify(args);
    return this.put(this._authOptions.apiUpdatePasswordPath, body);
  }

  //Сброс пароля
  resetPassword(resetPasswordData: ResetPasswordData): Observable<Response> {
    let body = JSON.stringify({
      email: resetPasswordData.email,
      redirect_url: this._authOptions.resetPasswordCallbackPath
    });

    return this.post(this._authOptions.apiResetPasswordPath, body);
  }

  //--------------------------------------------Private Methods---------------------------------------------------------

  //Проверка данных хранилища и обновление состояния если хранилище обновилось
  private _processAuthDataChecking() {
    if (this._authOptions.authDataChecking) {
      this._authDataInterval = setInterval(() => {
        let authData: AuthData = {
          accessToken: this._authOptions.dataStorage.getItem('accessToken'),
          client: this._authOptions.dataStorage.getItem('client'),
          expiry: this._authOptions.dataStorage.getItem('expiry'),
          tokenType: this._authOptions.dataStorage.getItem('tokenType'),
          uid: this._authOptions.dataStorage.getItem('uid')
        };


        if (this._checkAuthData(authData) && this._authData != authData) {
          if (!this._authData) {
            this._loadAuthData();
          }
        }

        if (this._authData)
          if (!this._checkAuthData(authData) && this._authData != authData) {
            this._clearAuthData();
          }

      }, this._authOptions.authDataCheckingInterval);
    }
  }

  private _clearAuthData() {
    this._authOptions.dataStorage.removeItem('accessToken');
    this._authOptions.dataStorage.removeItem('client');
    this._authOptions.dataStorage.removeItem('expiry');
    this._authOptions.dataStorage.removeItem('tokenType');
    this._authOptions.dataStorage.removeItem('uid');

    this._authData = null;
    this._userData = null;
  }

  //Проверка на заполненность входимых данных AuthData
  private _checkAuthData(authData: AuthData): boolean {
    if (
      authData.accessToken != null &&
      authData.client != null &&
      authData.expiry != null &&
      authData.tokenType != null &&
      authData.uid != null
    ) {
      if (this._authData != null)
        return authData.expiry >= this._authData.expiry;
      else
        return true;
    } else {
      return false;
    }
  }

  //Получение параметров AuthData из URL
  private _getAuthDataFromParams() {
    if (this._activatedRoute.queryParams)
      this._activatedRoute.queryParams.subscribe(queryParams => {
        let authData: AuthData = {
          accessToken: queryParams['token'] || queryParams['auth_token'],
          client: queryParams['client_id'],
          expiry: queryParams['expiry'],
          tokenType: 'Bearer',
          uid: queryParams['uid']
        };

        if (this._checkAuthData(authData))
          this._setAuthData(authData);
      });
  }

  //Получение параметров AuthData из хранилища
  private _getAuthDataFromStorage() {
    let authData: AuthData = {
      accessToken: this._authOptions.dataStorage.getItem('accessToken'),
      client: this._authOptions.dataStorage.getItem('client'),
      expiry: this._authOptions.dataStorage.getItem('expiry'),
      tokenType: this._authOptions.dataStorage.getItem('tokenType'),
      uid: this._authOptions.dataStorage.getItem('uid')
    };

    if (this._checkAuthData(authData))
      this._authData = authData;
  }

  //Получение параметров AuthData из ответа сервера
  private _getAuthHeadersFromResponse(data: any): void {
    let headers = data.headers;

    let authData: AuthData = {
      accessToken: headers.get('access-token'),
      client: headers.get('client'),
      expiry: headers.get('expiry'),
      tokenType: headers.get('token-type'),
      uid: headers.get('uid')
    };

    this._setAuthData(authData);
  }

  //Загрузка AuthData
  private _loadAuthData() {
    this._getAuthDataFromStorage();

    if (this._activatedRoute)
      this._getAuthDataFromParams();

    if (this._authData)
      this.validateToken();
  }

  //Запись AuthData в хранилище
  private _setAuthData(authData: AuthData): void {
    if (this._checkAuthData(authData)) {
      this._authData = authData;
      this._authOptions.dataStorage.setItem('accessToken', authData.accessToken);
      this._authOptions.dataStorage.setItem('client', authData.client);
      this._authOptions.dataStorage.setItem('expiry', authData.expiry);
      this._authOptions.dataStorage.setItem('tokenType', authData.tokenType);
      this._authOptions.dataStorage.setItem('uid', authData.uid);
    }
  }

  //--------------------------------------------Path Constructors-------------------------------------------------------

  //Получение URL API сервера
  private _getApiPath(): string {
    let constructedPath = '';

    if (this._authOptions.apiPath != null)
      constructedPath += this._authOptions.apiPath + '/';

    return constructedPath;
  }

  //Получение путя к авторизации через социальную сеть
  private _getOAuthPath(oAuthType: string): string {
    let oAuthPath: string;

    oAuthPath = this._authOptions.oAuthPaths[oAuthType];
    if (oAuthPath == null)
      oAuthPath = `/auth/${oAuthType}`;

    return oAuthPath;
  }

  //Получения путя
  private _getOAuthUrl(oAuthPath: string, callbackUrl: string, windowType: string): string {
    let url: string;

    url = `${this._authOptions.oAuthBase}/${oAuthPath}`;
    url += `?omniauth_window_type=${windowType}`;
    url += `&auth_origin_url=${encodeURIComponent(callbackUrl)}`;

    return url;
  }

  //--------------------------------------------OAuth-------------------------------------------------------------------

  //Парсинг AuthData из POST запроса
  private _getAuthDataFromPostMessage(data: any): void {
    let authData: AuthData = {
      accessToken: data['auth_token'],
      client: data['client_id'],
      expiry: data['expiry'],
      tokenType: 'Bearer',
      uid: data['uid']
    };
    this._setAuthData(authData);
    this.validateToken();
  }

  private _requestCredentialsViaPostMessage(authWindow: any): Observable<any> {
    return Observable.create(windowObserver => {

      Observable.create(responseObserver => {
        let messageSubscription = Observable.fromEvent(window, 'message')
          .pluck('data')
          .filter(this._oAuthWindowResponseFilter)
          .subscribe(data => responseObserver.next(data));

        setInterval(() => {
          if (authWindow.closed) {
            messageSubscription.unsubscribe();
            responseObserver.complete();
          } else authWindow.postMessage('requestCredentials', '*');
        }, 1000)

      }).subscribe(
        data => {
          switch (data.message) {
            case 'deliverCredentials':
              this._getAuthDataFromPostMessage(data);
              windowObserver.next('ok');
              break;
            case 'authFailure':
              windowObserver.error('Произошла неизвестная ошибка');
              break;
          }
        },
        () => {
        },
        () => {
          windowObserver.complete()
        }
      );
    });
  }

  private _oAuthWindowResponseFilter(data: any): any {
    if (data.message == 'deliverCredentials' || data.message == 'authFailure')
      return data;
  }

  //--------------------------------------------HTTP Wrappers-----------------------------------------------------------
  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(this._mergeRequestOptionsArgs({
      url: this._getApiPath() + url,
      method: RequestMethod.Get
    }, options));
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(this._mergeRequestOptionsArgs({
      url: this._getApiPath() + url,
      method: RequestMethod.Post,
      body: body
    }, options));
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(this._mergeRequestOptionsArgs({
      url: this._getApiPath() + url,
      method: RequestMethod.Put,
      body: body
    }, options));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(this._mergeRequestOptionsArgs({
      url: this._getApiPath() + url,
      method: RequestMethod.Delete
    }, options));
  }

  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(this._mergeRequestOptionsArgs({
      url: this._getApiPath() + url,
      method: RequestMethod.Patch,
      body: body
    }, options));
  }

  head(path: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request({
      method: RequestMethod.Head,
      url: this._getApiPath() + path
    });
  }

  options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(this._mergeRequestOptionsArgs({
      url: this._getApiPath() + url,
      method: RequestMethod.Options
    }, options));
  }

  // Сборка и отправка HTTP запроса
  request(options: RequestOptionsArgs): Observable<Response> {

    let baseRequestOptions: RequestOptions;
    let baseHeaders: { [key: string]: string; } = this._authOptions.globalOptions.headers;

    // Merge auth headers to request if set
    if (this._authData != null) {
      (<any>Object).assign(baseHeaders, {
        'access-token': this._authData.accessToken,
        'client': this._authData.client,
        'expiry': this._authData.expiry,
        'token-type': this._authData.tokenType,
        'uid': this._authData.uid
      });
    }

    baseRequestOptions = new RequestOptions({
      headers: new Headers(baseHeaders)
    });

    baseRequestOptions = baseRequestOptions.merge(options);

    let response = this._http.request(new Request(baseRequestOptions)).share();

    this._handleResponse(response);

    return response;
  }

  private _mergeRequestOptionsArgs(options: RequestOptionsArgs, addOptions?: RequestOptionsArgs): RequestOptionsArgs {

    let returnOptions: RequestOptionsArgs = options;

    if (options)
      (<any>Object).assign(returnOptions, addOptions);

    return returnOptions;
  }

  //Проверка на выполненность запроса и обновление хранилища
  private _handleResponse(response: Observable<Response>): void {
    response.subscribe(res => {
      this._getAuthHeadersFromResponse(<any>res);
    }, error => {
      this._getAuthHeadersFromResponse(<any>error);
    });
  }

}
