import { TestBed, inject } from '@angular/core/testing';

import { DayReportService } from './day-report.service';

describe('DayReportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DayReportService]
    });
  });

  it('should ...', inject([DayReportService], (service: DayReportService) => {
    expect(service).toBeTruthy();
  }));
});
