import {Injectable} from '@angular/core';
import {AuthService} from "./auth/auth.service";
import {Observable} from "rxjs/Observable";
import {DayReport} from "../models/day-report.model";
import {handleError} from "../shared/functions";

declare let moment: any;

@Injectable()
export class DayReportService {

  constructor(private _authService: AuthService) {
  }

  data(day: string = ''): Observable<DayReport[]> {
    const report_day: string = (day) ? day : moment().format('L');

    return this._authService.get(`day_reports?report_day=${report_day}`)
      .map(result => result.json().map(record => new DayReport().serialize(record)))
      .catch(handleError)
  }

  references(): Observable<any> {
    return this._authService.get('day_reports/references')
      .map(result => result.json())
      .catch(handleError)
  }

  update(record: DayReport): Observable<DayReport> {
    return this._authService.put(`day_reports/${record.day_report_id}`, record)
      .map(result =>result.json())
      .catch(handleError)
  }

  create(record: DayReport): Observable<DayReport> {
    return this._authService.post('day_reports', record)
      .map(result => result.json())
      .catch(handleError)
  }

  destroy(id: number): Observable<any> {
    return this._authService.delete(`day_reports/${id}`)
      .map(result => null)
      .catch(handleError)
  }

}
